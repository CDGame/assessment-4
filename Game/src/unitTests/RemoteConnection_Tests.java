package unitTests;

import static org.junit.Assert.*;
import multiplayer.RemoteConnection;
import multiplayer.Match;
import logicClasses.Airspace;
import logicClasses.Controls;
import logicClasses.Flight;
import org.junit.Test;
import org.junit.Before;

public class RemoteConnection_Tests {

	private RemoteConnection conn;
	private int userID, hostID;
	private Match match;
	private Flight flight;
	private Airspace airspace;

	@Before
	public void setUp() {
		conn = new RemoteConnection();
	}

	@Test //RC001
	public void createNewUser() {
		// Testing that the function returns the ID of the newly created user and the difference between the userID is 1 because of the auto increment in the database.
		userID = conn.newUser("Test");
		hostID = conn.newUser("Test2");
		assertTrue(userID > 0);
		assertTrue(hostID > 0);
		assertEquals(hostID - this.userID, 1);
	}
	
	@Test //RC002
	public void createNewHost() {
		// Testing that the function creates a new hosted game
		hostID = conn.newUser("Test2");
		conn.newHost(hostID, 2);
		assertTrue(conn.getHosts(userID)[0] != null);
	}
	
	@Test //RC003
	public void joinGame() {
		// Testing that the player joins the game successfully and a new game is created
		userID = conn.newUser("Test");
		hostID = conn.newUser("Test2");
		conn.joinGame(hostID, userID);
		conn.createGame(hostID);
		assertTrue(conn.gameExists(userID));
	}
	
//	@Test
//	public void sendFlight() {
//		// Testing that an aircraft is sent to the other airspace successfully with the same name, X and Y coordinates
//		userID = conn.newUser("Test");
//		hostID = conn.newUser("Test2");
//		this.newMatch(userID, hostID);
//		match = conn.getMatchInfo(userID, 0, 0);
//		airspace = new Airspace();
//
//		airspace.setDifficultyValueOfGame(3);
//		airspace.getControls().setDifficultyValueOfGame(Controls.HARD);
//		flight = new Flight(airspace);
//		flight.setFlightName("Test-Flight");
//		conn.sendFlight(hostID, flight, flight.getX(), flight.getY());
//		assertTrue(match.isIncomingAircraft());
//		assertTrue(match.getFlightName() == "Test-Flight");
//		assertTrue(match.getFlightX() == flight.getX());
//		assertTrue(match.getFlightY() == flight.getY());
//	}	

	@Test //RC004
	public void startTimer() {
		// Testing that the timer starts and it is in the range between one minute of time
		userID = conn.newUser("Test");
		hostID = conn.newUser("Test2");
		this.newMatch(userID, hostID);
		conn.startTimer(userID);
		match = conn.getMatchInfo(userID, 0, 0);
		assertTrue(match.isTimerOn());
		assertTrue(match.getTimer() >= 0 && match.getTimer() <= 60);
	}	

	@Test //RC005
	public void scoreTest() {
		// Testing that the score and remaining aircrafts are being set correctly
		userID = conn.newUser("Test");
		hostID = conn.newUser("Test2");
		this.newMatch(userID, hostID);
		match = conn.getMatchInfo(hostID, 100, 6);
		match = conn.getMatchInfo(userID, 0, 0);
		assertEquals(match.getPlayers()[0].getScore(), 100);
		assertEquals(match.getPlayers()[0].getRemainingAircrafts(), 6);
	}	
	
	@Test //RC006
	public void setLost() {
		// Testing that the losing flag for a player is set
		userID = conn.newUser("Test");
		hostID = conn.newUser("Test2");
		this.newMatch(userID, hostID);
		conn.setLost(userID);
		match = conn.getMatchInfo(hostID, 0, 0);
		assertTrue(match.getPlayers()[0].lost());
	}
	
	// Method for setting up a new match
	private void newMatch(int userID, int hostID){
		conn.newHost(hostID, 2);
		conn.joinGame(hostID, userID);
		conn.createGame(hostID);		
	}
	
}
