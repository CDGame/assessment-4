package unitTests;

import static org.junit.Assert.*;
import logicClasses.Airspace;
import logicClasses.Controls;

import org.junit.Before;
import org.junit.Test;
import states.PlayState;

public class PlayState_Test {
	
	private Airspace airspaceInstance;
	private PlayState playStateInstance;
	private int playerNumber, playerScore, difficulty;
	private String player2Name, player3Name, player4Name;
	

	@Before
	public void setup() {
		int playState = 2;
		playStateInstance = new PlayState(playState);
		airspaceInstance = new Airspace();
		playerNumber = 1;
		playerScore = 0;
		difficulty = 0;
		player2Name = "Player2";
		player3Name = "Player3";
		player4Name = "Player4";
	}

	@Test
	public void testGetID() {
		int actualID = playStateInstance.getID();
		assertEquals(2, actualID);
	}

//	@Test
//	public void testPlayState() {
//		fail("Not yet implemented");
//	}

//	@Test
//	public void testInit() throws SlickException {
//		fail("Not yet implemented");
//	}

//	@Test
//	public void testRender() {
//		fail("Not yet implemented");
//	}

//	@Test
//	public void testUpdate() {
//		fail("Not yet implemented");
//	}

	// This test also tests getAirspace
	@Test
	public void testSetAirspace() {
		Airspace airspace = new Airspace();
		playStateInstance.setAirspace(airspaceInstance);

		Airspace actualAirspace = PlayState.getAirspace();
		assertEquals(airspace.toString(), actualAirspace.toString());
	}

	
	@Test // PS001
	public void extPlayerScoreStringTest(){
		playerNumber = 2;
		playerScore = 10;
		assert(playStateInstance.extPlayerScoreString(playerNumber, playerScore) == "Player2 = 10");
		
		playerNumber = 3;
		playerScore = 20;
		assert(playStateInstance.extPlayerScoreString(playerNumber, playerScore) == "Player3 = 20");
		
		playerNumber = 4;
		playerScore = 45;
		assert(playStateInstance.extPlayerScoreString(playerNumber, playerScore) == "Player4 = 45");
	}

	
	@Test // PS002
	public void prepareAirspaceTest(){
		difficulty = 1;
		playStateInstance.prepareAirspace(airspaceInstance, difficulty);
		assert(airspaceInstance.getDifficultyValueOfGame() == 1);
		assert(airspaceInstance.getControls().getDifficultyValueOfGame() == Controls.EASY);
	
		difficulty = 2;
		playStateInstance.prepareAirspace(airspaceInstance, difficulty);
		assert(airspaceInstance.getDifficultyValueOfGame() == 2);
		assert(airspaceInstance.getControls().getDifficultyValueOfGame() == Controls.NORMAL);
		
		difficulty = 3;
		playStateInstance.prepareAirspace(airspaceInstance, difficulty);
		assert(airspaceInstance.getDifficultyValueOfGame() == 3);
		assert(airspaceInstance.getControls().getDifficultyValueOfGame() == Controls.HARD);
	}
}
