package states;

import java.awt.Font;
import java.io.InputStream;

import logicClasses.Achievements;
import logicClasses.Airspace;
import logicClasses.Controls;
import logicClasses.Flight;
import logicClasses.ScoreTracking;
import multiplayer.*;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.loading.LoadingList;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.ResourceLoader;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.Image;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.geom.Rectangle;

import util.DeferredFile;

public class PlayState extends BasicGameState {
	/*
	 *  Aesthetic variables
	 */
	private static Image backgroundImage, difficultyBackground,
	statusBarImage, clockImage, windImage, flightIcon, multiplayerBackground, connectingBackground, joiningBackground, hostingBackground;
	private static Sound endOfGameSound;
	private static Music gameplayMusic;
	private static TrueTypeFont font, panelFont, buttonFont;
	private String fontPath = "res/blue_highway_font/bluebold.ttf";
	private static Color buttonNorm, buttonHover;
	private UnicodeFont uFont = null;
	private TextField usernameField;
	private Rectangle button;

	/*
	 *  Functional variables
	 */
	private StateBasedGame sbg;

	// Standard game variables
	private static Airspace airspace;
	public static float time;
	private String stringTime;
	private boolean settingDifficulty, gameEnded, settingGameMode;
	private static boolean multiplayer;
	private Achievements achievement;
	private String achievementMessage = "";

	// Multiplayer variables
	private static RemoteConnection remote = new RemoteConnection();
	private boolean  hosting, joining, connected, hostingWait, joiningWait, connect;
	private static int userID;
	private String username;
	private static String player2Name;
	private static String player3Name;
	private static String player4Name;
	private int numberOfPlayers, multiplayerOffset;
	private Host[] hosts;
	private int hostNumber = 0;
	private static Match match;
	private int iterator = 0; // used for counting how many frames have passed since last connection to the server
	private static int numberOfAircraftToGenerate;


	public PlayState(int state) {
		achievement = new Achievements();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void init(GameContainer gc, StateBasedGame sbg)
			throws SlickException {

		//TODO change to enumer
		gameEnded = false;
		settingGameMode = true;
		settingDifficulty = false;
		hosting = false;
		joining = false;
		connected = false;
		connect = false;
		hostingWait = false;
		joiningWait = false;
		multiplayerOffset = 20;
		this.sbg = sbg;
		numberOfAircraftToGenerate = 0;

		time = 0;
		airspace = new Airspace();
		this.stringTime = "";

		uFont = new UnicodeFont(fontPath,30,false,false);
		uFont.addAsciiGlyphs();   //Add Glyphs
		uFont.getEffects().add(new ColorEffect(java.awt.Color.WHITE));
		uFont.loadGlyphs();  //Load Glyphs
		usernameField = new TextField(gc, uFont, 450, 350, 300, 50);
		player2Name = "";
		player3Name = "";
		player4Name = "";

		gc.setAlwaysRender(true);
		gc.setUpdateOnlyWhenVisible(true);

		// Font

		{
			buttonNorm = new Color(255,255,255);
			buttonHover = new Color(200,200,200);

			LoadingList loading = LoadingList.get();

			loading.add(new DeferredFile("res/blue_highway_font/bluehigh.ttf") {
				public void loadFile(String filename) {
					InputStream inputStream = ResourceLoader
							.getResourceAsStream(filename);
					try {
						Font awtFont = Font.createFont(Font.TRUETYPE_FONT,
								inputStream);
						font = new TrueTypeFont(awtFont.deriveFont(20f), true);
						panelFont = new TrueTypeFont(awtFont.deriveFont(14f),
								true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			loading.add(new DeferredFile("res/a_love_of_thunder_font/A Love of Thunder.ttf") {
				public void loadFile(String filename) {
					InputStream inputStream = ResourceLoader
							.getResourceAsStream(filename);
					try {
						Font awtFont = Font.createFont(Font.TRUETYPE_FONT,
								inputStream);
						buttonFont = new TrueTypeFont(awtFont.deriveFont(32f), true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			// Music
			loading.add(new DeferredFile("res/music/new/muzikele.ogg") {
				public void loadFile(String filename) throws SlickException {
					gameplayMusic = new Music(filename);
				}
			});

			loading.add(new DeferredFile("res/music/new/Big Explosion.ogg") {
				public void loadFile(String filename) throws SlickException {
					endOfGameSound = new Sound(filename);
				}
			});

			// Images
			loading.add(new DeferredFile(
					"res/graphics/new/control_bar_vertical.png") {
				public void loadFile(String filename) throws SlickException {
					statusBarImage = new Image(filename);
				}
			});

			loading.add(new DeferredFile("res/graphics/clock.png") {
				public void loadFile(String filename) throws SlickException {
					clockImage = new Image(filename);
				}
			});

			loading.add(new DeferredFile("res/graphics/new/wind_indicator.png") {
				public void loadFile(String filename) throws SlickException {
					windImage = new Image(filename);
				}
			});

			loading.add(new DeferredFile(
					"res/graphics/new/control_bar_plane.png") {
				public void loadFile(String filename) throws SlickException {
					flightIcon = new Image(filename);
				}
			});

			loading.add(new DeferredFile("res/graphics/new/background.png") {
				public void loadFile(String filename) throws SlickException {
					backgroundImage = new Image(filename);
				}
			});

			loading.add(new DeferredFile("res/graphics/Menu/new/difficulty.png") {
				public void loadFile(String filename) throws SlickException {
					difficultyBackground = new Image(filename);
				}
			});


			loading.add(new DeferredFile("res/graphics/Menu/new/menu_screen.png") {
				public void loadFile(String filename) throws SlickException {
					multiplayerBackground = new Image(filename);
				}
			});

			loading.add(new DeferredFile("res/graphics/Menu/new/menu_screen.png") {
				public void loadFile(String filename) throws SlickException {
					connectingBackground = new Image(filename);
				}
			});

			loading.add(new DeferredFile("res/graphics/Menu/new/menu_screen.png") {
				public void loadFile(String filename) throws SlickException {
					joiningBackground = new Image(filename);
				}
			});

			loading.add(new DeferredFile("res/graphics/Menu/new/menu_screen.png") {
				public void loadFile(String filename) throws SlickException {
					hostingBackground = new Image(filename);
				}
			});

		}

		// initialise the airspace object;
		// Waypoints
		airspace.newWaypoint(350, 150, "A");
		airspace.newWaypoint(400, 470, "B");
		airspace.newWaypoint(700, 60, "C");
		airspace.newWaypoint(800, 320, "D");
		airspace.newWaypoint(600, 418, "E");
		airspace.newWaypoint(500, 220, "F");
		airspace.newWaypoint(950, 188, "G");
		airspace.newWaypoint(1050, 272, "H");
		airspace.newWaypoint(900, 420, "I");
		airspace.newWaypoint(240, 250, "J");

		// EntryPoints
		airspace.newEntryPoint(150, 400); 		// left
		airspace.newEntryPoint(1200, 200); 		// right
		airspace.newEntryPoint(600, 0); 		// top
		airspace.newEntryPoint(960, 355); 		// airport1
		airspace.newEntryPoint(530, 455); 		// airport2

		// Exit Points
		airspace.newExitPoint(800, 0, "1"); 	//top
		airspace.newExitPoint(150, 200, "2"); 	//left
		airspace.newExitPoint(1200, 300, "3");	// right
		airspace.newExitPoint(790, 145, "4"); 	// airport1
		airspace.newExitPoint(360, 245, "5"); 	// airport2


		airspace.init(gc);
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)
			throws SlickException {
		/*
		 *  When the user is waiting for other players to join a multiplayer game they are hosting
		 */
		if (hostingWait)
		{
			g.setColor(Color.white);
			Mouse.getX();
			hostingBackground.draw(0, 0);

			g.drawString("Wait for the players to join the game!", 420, 300);

			return;
		}

		/*
		 *  When the user is waiting for other players to join a multiplayer game they have joined
		 */
		else if (joiningWait)
		{
			g.setColor(Color.white);
			Mouse.getX();
			joiningBackground.draw(0, 0);

			g.drawString("Wait for the other players to join the game!", 420, 300);

			return;
		}

		/*
		 * 	When the user is setting up a multiplayer game that they are hosting
		 *  Occurs immeditately after the user has chosen the "Host" option within the multiplayer game mode
		 */
		else if (hosting)
		{
			int posX = Mouse.getX();
			int posY = stateContainer.Game.MAXIMUMHEIGHT - Mouse.getY();
			// Fixing posY to reflect graphics coords

			hostingBackground.draw(0, 0);

			g.drawString("Select how many players will play the game:", 420, 300);

			if (posX > 530 && posX < 672 && posY > 350 && posY < 388)
				buttonFont.drawString(530, 350, "2 players", buttonHover);
			else
				buttonFont.drawString(530, 350, "2 players", buttonNorm);

			if (posX > 530 && posX < 672 && posY > 450 && posY < 488)
				buttonFont.drawString(530, 450, "3 players", buttonHover);
			else
				buttonFont.drawString(530, 450, "3 players", buttonNorm);

			if (posX > 530 && posX < 672 && posY > 550 && posY < 588)
				buttonFont.drawString(530, 550, "4 players", buttonHover);
			else
				buttonFont.drawString(530, 550, "4 players", buttonNorm);

			if (posX > 20 && posX < 136 && posY > 20 && posY < 66) 
				buttonFont.drawString(20, 20, "Menu", buttonHover);
			else 
				buttonFont.drawString(20, 20, "Menu", buttonNorm);			

			return;
		}

		/*	When the user is choosing which existing multiplayer game to join
		 *  Occurs immeditately after the user has chosen the "Join" option within the multiplayer game mode
		 */
		else if (joining)
		{
			joiningBackground.draw(0, 0);

			int posX = Mouse.getX();
			int posY = stateContainer.Game.MAXIMUMHEIGHT - Mouse.getY();
			// Fixing posY to reflect graphics coords			hostingBackground.draw(0, 0);

			g.setColor(Color.white);
			g.drawString("Select which game you want to join:", 420, 230);

			//<=1 because when you go to this page the iterator is set to 0 on the first frame and then increased in update() as it is called first
			if(iterator <= 1)
				hosts = remote.getHosts(userID);

			g.setColor(Color.black);
			for (int i = 0; i < hosts.length; i++) {
				if(hosts[i] != null){
					button = new Rectangle(460, 250 + 50*i, 200, 40);
					g.draw(button);
					g.fillRect(460, 250 + 50*i, 200, 40, new Image("res/graphics/new/button.png"), 0, 0);
					g.drawString(hosts[i].getName(), 470, 260 + 50*i);
					g.drawString(hosts[i].getPlayersJoined() + "/" + hosts[i].getTotalPlayers(), 630, 260 + 50*i);
				}
			}

			if (hosts[0] == null)
				font.drawString(470, 400,"No games currently available", buttonNorm);


			if (posX > 20 && posX < 136 && posY > 20 && posY < 66) 
				buttonFont.drawString(20, 20, "Menu", buttonHover);
			else 
				buttonFont.drawString(20, 20, "Menu", buttonNorm);

			return;
		}

		/*
		 * When the user is selecting whether to host or join a multiplayer game within the multiplayer game mode
		 */
		else if (connected)
		{
			int posX = Mouse.getX();
			int posY = stateContainer.Game.MAXIMUMHEIGHT - Mouse.getY();
			// Fixing posY to reflect graphics coords

			connectingBackground.draw(0, 0);

			if (posX > 530 && posX < 632 && posY > 350 && posY < 388)
				buttonFont.drawString(530, 350, "Host", buttonHover);
			else
				buttonFont.drawString(530, 350, "Host", buttonNorm);

			if (posX > 530 && posX < 632 && posY > 450 && posY < 488)
				buttonFont.drawString(530, 450, "Join", buttonHover);
			else
				buttonFont.drawString(530, 450, "Join", buttonNorm);

			if (posX > 20 && posX < 136 && posY > 20 && posY < 66) 
				buttonFont.drawString(20, 20, "Menu", buttonHover);
			else 
				buttonFont.drawString(20, 20, "Menu", buttonNorm);

			return;
		}

		/*
		 * When the user is entering their username after choosing the multiplayer game mode
		 * This occurs before the user has chosen whether to join or host a game
		 */
		else if(connect)
		{

			int posX = Mouse.getX();
			int posY = stateContainer.Game.MAXIMUMHEIGHT - Mouse.getY();
			// Fixing posY to reflect graphics coords

			connectingBackground.draw(0, 0);

			g.drawString("Enter your name:", 520, 300);
			usernameField.render(gc, g);

			if (posX > 530 && posX < 672 && posY > 450 && posY < 488)
				buttonFont.drawString(530, 450, "Connect", buttonHover);
			else
				buttonFont.drawString(530, 450, "Connect", buttonNorm);

			if (posX > 20 && posX < 136 && posY > 20 && posY < 66) 
				buttonFont.drawString(20, 20, "Menu", buttonHover);
			else 
				buttonFont.drawString(20, 20, "Menu", buttonNorm);
			return;
		}	

		/*
		 * When the user is choosing between single player and multiplayer game modes
		 */
		else if (settingGameMode)
		{
			int posX = Mouse.getX();
			int posY = stateContainer.Game.MAXIMUMHEIGHT - Mouse.getY();
			// Fixing posY to reflect graphics coords

			multiplayerBackground.draw(0, 0);

			if (posX > 490 && posX < 850 && posY > 320 && posY < 370)
				buttonFont.drawString(490, 320, "Single Player", buttonHover);
			else
				buttonFont.drawString(490, 320, "Single Player", buttonNorm);

			if (posX > 500 && posX < 850 && posY > 450 && posY < 490)
				buttonFont.drawString(500, 450, "Multiplayer", buttonHover);
			else
				buttonFont.drawString(500, 450, "Multiplayer", buttonNorm);

			if (posX > 20 && posX < 136 && posY > 20 && posY < 66) 
				buttonFont.drawString(20, 20, "Menu", buttonHover);
			else 
				buttonFont.drawString(20, 20, "Menu", buttonNorm);

			return;
		}

		/*
		 * When the user is choosing the game difficulty within single player mode
		 */
		else if (settingDifficulty) {

			int posX = Mouse.getX();
			int posY = stateContainer.Game.MAXIMUMHEIGHT - Mouse.getY();
			// Fixing posY to reflect graphics coords

			difficultyBackground.draw(0, 0);

			if (posX > 100 && posX < 216 && posY > 300 && posY < 354)
				buttonFont.drawString(100, 300, "Easy", buttonHover);
			else
				buttonFont.drawString(100, 300, "Easy", buttonNorm);

			if (posX > 100 && posX < 284 && posY > 400 && posY < 454)
				buttonFont.drawString(100, 400, "Medium", buttonHover);
			else
				buttonFont.drawString(100, 400, "Medium", buttonNorm);

			if (posX > 100 && posX < 227 && posY > 500 && posY < 554)
				buttonFont.drawString(100, 500, "Hard", buttonHover);
			else
				buttonFont.drawString(100, 500, "Hard", buttonNorm);

			if (posX > 20 && posX < 136 && posY > 20 && posY < 66) 
				buttonFont.drawString(20, 20, "Menu", buttonHover);
			else 
				buttonFont.drawString(20, 20, "Menu", buttonNorm);
			return;
		}


		/*
		 *	The main game 
		 */
		else {
			// set font for the rest of the render
			g.setFont(font);

			// Drawing Side Images
			backgroundImage.draw(150, 0);
			statusBarImage.draw(0, 0);

			// Drawing Airspace and elements within it
			g.setColor(Color.white);
			airspace.render(g, gc);

			// Drawing Clock and Time
			g.setColor(Color.white);
			clockImage.draw(0, 0);
			g.drawString(stringTime, 25, 5);

			// Drawing Score
			g.drawString(Airspace.getScore().toString(), 10, 40);
			int playerNo = 0;
			if(multiplayer){
				if(match != null)
					for (Player player: match.getPlayers()){
						if (player != null){
							if(player.lost() == true){
								g.setColor(Color.red);
							}else{
								g.setColor(Color.white);																
							}
							if (player.getName().length() <=5)
								g.drawString(player.getName() + " = " + player.getScore(), 10, 55 + 15*playerNo);
							else
								g.drawString(player.getName().substring(0, 5) + " = " + player.getScore(), 10, 55 + 15*playerNo);
							playerNo += 1;
						}

					}
				g.setColor(Color.white);
				
				// Drawing number of flights remaining or the timer
				if(match != null && match.isTimerOn() == true){
					int timeLeft = (int) Math.ceil(match.getTimer() - iterator/60);
					if (timeLeft < 0)
						timeLeft = 0;
					g.drawString("Time left: "+  timeLeft, 10, 23);										
				}else{
					int totalFlightsLeft = numberOfAircraftToGenerate;
					if(match != null){
						Player[] players = match.getPlayers();
						for(int i = 0; i < match.getNoOfPlayers() - 1; i++){
							totalFlightsLeft += players[i].getRemainingAircrafts();
						}					
					}
					g.drawString("Flights left: "+numberOfAircraftToGenerate+"/"+totalFlightsLeft, 10, 23);					
				}
			}


			{ // draw flight information panels
				int baseY = 60;
				if (multiplayer)
					if(match == null)
						baseY += multiplayerOffset*numberOfPlayers;
					else
						baseY += multiplayerOffset*match.getNoOfPlayers();
				for (Flight f : airspace.getListOfFlights()) {
					renderFlightPanel(f, g, baseY);
					baseY += 50;
				}
			}

			// drawing wind direction
			windImage.setRotation(windImage.getRotation()
					+ ((float) Math.cos(time / 2999.0) + (float) Math
							.sin(time / 1009.0)) / 3);
			// for now, set wind direction pseudo-randomly
			windImage.draw(14, 550);
			g.drawString("Wind:", 60, 550);
			g.drawString(String.valueOf(Math.round(windImage.getRotation())),
					65, 565);

			// Drawing Achievements
			g.drawString(
					Airspace.getScore().scoreAchievement(),
					stateContainer.Game.MAXIMUMWIDTH
					- font.getWidth(Airspace.getScore()
							.scoreAchievement()) - 10, 30);
			g.drawString(achievementMessage, stateContainer.Game.MAXIMUMWIDTH
					- 10 - font.getWidth(achievementMessage), 40);
		}
	}

	private void renderFlightPanel(Flight f, Graphics g, int baseY) {
		// draw border if flight is selected
		if (f.isSelected()) {
			g.drawRoundRect(1, baseY, 135, 50, 3);
		}

		int h = panelFont.getHeight();

		// draw icon, rotated to match plane
		flightIcon.setRotation((float) f.getCurrentHeading());
		flightIcon.draw(7, 7 + baseY);
		// draw flight name at bottom of box
		panelFont.drawString(4, 50 - h + baseY, f.getFlightName());

		String[] data = new String[] {
				"Plan: " + f.getFlightPlan().toString(),
				"Speed: " + String.valueOf(Math.round(f.getVelocity()))
				+ " mph",
				"Altitude: "
						+ String.valueOf(Math.round(f.getCurrentAltitude()))
						+ " ft" };

		baseY = baseY + 3;
		for (String str : data) {
			panelFont.drawString(40, baseY, str);
			baseY += h;
		}

	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta)
			throws SlickException {


		//Increases frames passed since last connection
		if(multiplayer){
			iterator++;
			if(iterator >= 300)
				iterator = 0;
		}

		/*
		 *  When the user is waiting for other players to join a multiplayer game they are hosting
		 */
		if (hostingWait)
		{
			//System.out.println(userID + " " + sql.gameExists(userID));
			if (iterator == 0 && remote.gameExists(userID) == true){
				hostingWait = false;
				this.prepareAirspace(airspace, 3);
			}

			return;
		}

		/*
		 *  When the user is waiting for other players to join a multiplayer game they have joined
		 */
		else if (joiningWait)
		{
			//System.out.println(userID + " " + sql.gameExists(userID));
			if (iterator == 0 && remote.gameExists(userID) == true){
				joiningWait = false;
				this.prepareAirspace(airspace, 3);
			}

			return;
		}

		/*
		 * 	When the user is setting up a multiplayer game that they are hosting
		 *  Occurs immeditately after the user has chosen the "Host" option within the multiplayer game mode
		 */
		else if (hosting)
			return;

		/*	When the user is choosing which existing multiplayer game to join
		 *  Occurs immeditately after the user has chosen the "Join" option within the multiplayer game mode
		 */
		else if (joining)
			return;

		/*
		 * When the user is selecting whether to host or join a multiplayer game within the multiplayer game mode
		 */
		else if (connected) 
			return;

		/*
		 * When the user is entering their username after choosing the multiplayer game mode
		 * This occurs before the user has chosen whether to join or host a game
		 */
		else if (connect) 
			return;

		/*
		 * Checks if the game has been retried and if it has resets the airspace
		 */
		else if (gameEnded) {

			airspace.resetAirspace();
			time = 0;
			gameEnded = false;
			settingGameMode = true;
			settingDifficulty = false;
			Airspace.getScore().resetScore();
		}

		/*
		 * When the user is choosing between single player and multiplayer game modes
		 */
		else if (settingGameMode) { 

		}

		/*
		 * When the user is choosing the game difficulty within single player mode
		 */
		else if (settingDifficulty) {

		}

		/*
		 * The main game
		 */
		else {
			// Gets match info	
			if(multiplayer && iterator <= 1){
				match = remote.getMatchInfo(userID, ScoreTracking.getScore(), Airspace.aircraftsRemaining());
				//Check if all players have generated all the aircrafts
				if(match.isTimerOn() == false && Airspace.aircraftsRemaining() < 1){
					boolean turnOnTimer = true;
					Player[] players = match.getPlayers();
					for(int i = 0; i < match.getNoOfPlayers() - 1; i++){
						if(players[i].getRemainingAircrafts() > 0)
							turnOnTimer = false;
					}
					if(turnOnTimer){
						remote.startTimer(userID);
					}
				}
			}

			// Updating Clock and Time

			time += delta;
			achievement.timeAchievement((int) time);
			float decMins = time / 1000 / 60;
			int mins = (int) decMins;
			float decSecs = decMins - mins;

			int secs = Math.round(decSecs * 60);

			String stringMins = "";
			String stringSecs = "";
			if (secs >= 60) {
				secs -= 60;
				mins += 1;
				// {!} should do +60 score every minute(possibly)
				// - after 3 minutes adds on 2 less points every time?
				Airspace.getScore().updateTimeScore();
			}
			if (mins < 10) {
				stringMins = "0" + mins;
			} else {
				stringMins = String.valueOf(mins);
			}
			if (secs < 10) {
				stringSecs = "0" + secs;
			} else {
				stringSecs = String.valueOf(secs);
			}

			this.stringTime = stringMins + ":" + stringSecs;

			// Updating Aircraft generated counter
			numberOfAircraftToGenerate = Airspace.getFlightsRemainingToGenerate();


			// Updating Airspace

			airspace.newFlight(gc);
			airspace.update(gc);
			if (airspace.getSeparationRules().getGameOverViolation() == true || multiplayer == true && (match.isVictory() == true || match.isTimerOn() == true && match.getTimer() < 1)) {
				achievementMessage = achievement.crashAchievement((int) time); 
				// pass the game time as of game over into the crashAchievement

				airspace.getSeparationRules().setGameOverViolation(false);
				airspace.resetAirspace();
				gameplayMusic.stop();
				endOfGameSound.play();
				sbg.enterState(stateContainer.Game.GAMEOVERSTATE);
				gameEnded = true;

			}

			Input input = gc.getInput();

			// Checking For Pause Screen requested in game

			if (input.isKeyPressed(Input.KEY_P)) {
				sbg.enterState(stateContainer.Game.PAUSESTATE);
			}

			if (!gameplayMusic.playing()) {
				// Loops gameplay music based on random number created in init

				gameplayMusic.loop(1.0f, 0.5f);
			}
		}

	}

	@Override
	public void mouseClicked(int button, int posX, int posY, int clicks) {


		if (hosting)
		{
			if ((posX > 530 && posX < 672 && posY > 350 && posY < 388)) {
				hosting = false;
				hostingWait = true;
				numberOfPlayers = 2;
				remote.newHost(userID, numberOfPlayers);

			}
			else if ((posX > 530 && posX < 672 && posY > 450 && posY < 488)) {
				hosting = false;
				hostingWait = true;
				numberOfPlayers = 3;
				remote.newHost(userID, numberOfPlayers);
			}
			else if ((posX > 530 && posX < 672 && posY > 550 && posY < 588)) {
				hosting = false;
				hostingWait = true;
				numberOfPlayers = 4;
				remote.newHost(userID, numberOfPlayers);
			}
			else if (posX > 20 && posX < 136 && posY > 20 && posY < 66) {
				settingGameMode = true;
				hosting = false;
				connected = false;
				usernameField.setText("");
				username = "";
				sbg.enterState(stateContainer.Game.MAINMENUSTATE);
			}


			return;
		}

		else if (joining)
		{
			System.out.println("X:" + posX + " Y:" + posY);
			if (posX > 460 && posX < 660) {
				if(posY > 250 && posY < 290)
					hostNumber = 1;
				else if(posY > 300 && posY < 340)
					hostNumber = 2;
				else if(posY > 350 && posY < 390)
					hostNumber = 3;
				else if(posY > 400 && posY < 440)
					hostNumber = 4;
				else if(posY > 450 && posY < 490)
					hostNumber = 5;
				else if(posY > 500 && posY < 540)
					hostNumber = 6;
				else if(posY > 550 && posY < 590)
					hostNumber = 7;
			}
			if(hostNumber > 0){
				hosts = remote.getHosts(userID);
				// Checks whether the game you are about to join exists
				if (hosts[0] == null)
					return;
				else{
					int hostID = hosts[hostNumber - 1].getID(); //causes error
					if(hostID > 0){
						// Checks whether this is the last player joining the hosted game
						if (hosts[hostNumber - 1].getPlayersJoined() + 1 >= hosts[hostNumber - 1].getTotalPlayers()){
							joining = false;
							remote.joinGame(hostID, userID);
							remote.createGame(hostID);
							this.prepareAirspace(airspace, 3);
						}
						else{
							joining = false;
							joiningWait = true;
							remote.joinGame(hostID, userID);
						}
					}
				}
			}
			else if (posX > 20 && posX < 136 && posY > 20 && posY < 66) {
				settingGameMode = true;
				joining = false;
				connected = false;
				joiningWait = false;
				usernameField.setText("");
				username = "";
				sbg.enterState(stateContainer.Game.MAINMENUSTATE);
			}

			return;
		}

		else if (connected){


			if ((posX > 530 && posX < 652) && (posY > 350 && posY < 388)) {
				connected = false;
				hosting = true;
			}
			else if ((posX > 530 && posX < 652) && (posY > 450 && posY < 488)) {
				connected = false;
				joining = true;
				iterator = 0; //Resets iterator so that hosts could be received from the server on the first frame
			}
			else if (posX > 20 && posX < 136 && posY > 20 && posY < 66) {
				settingGameMode = true;					
				connected = false;
				connect = false;
				hostingWait = false;
				joiningWait = false;
				usernameField.setText("");
				username = "";
				sbg.enterState(stateContainer.Game.MAINMENUSTATE);
			}



			return;
		}

		else if (connect) {


			if ((posX > 530 && posX < 652) && (posY > 450 && posY < 488)) {
				connected = true;
				connect = false;
				username = usernameField.getText().replace(" ", "_");
				if (username == "")
					username = "Guest";
				for (int i = 0; i < username.length(); i++){
					if (((username.charAt(i)) != Character.LOWERCASE_LETTER) && 
							((username.charAt(i)) != Character.UPPERCASE_LETTER) && (username.charAt(i) != '*')){
						username.replace(username.charAt(i), '*');
					}
				}
				userID = remote.newUser(username);
			}
			else if (posX > 20 && posX < 136 && posY > 20 && posY < 66) {
				settingGameMode = true;
				connected = false;
				connect = false;
				sbg.enterState(stateContainer.Game.MAINMENUSTATE);
			}

			return;
		}

		else if (settingDifficulty) {
			if ((posX > 100 && posX < 216) && (posY > 300 && posY < 354)) {
				this.prepareAirspace(airspace, 1);
				settingDifficulty = false;
			}

			else if ((posX > 100 && posX < 284) && (posY > 400 && posY < 454)) {
				this.prepareAirspace(airspace, 2);
				settingDifficulty = false;
			}

			else if ((posX > 100 && posX < 227) && (posY > 500 && posY < 554)) {
				this.prepareAirspace(airspace, 3);
				settingDifficulty = false;
			}

			else if (posX > 20 && posX < 136 && posY > 20 && posY < 66) {
				settingGameMode = true;
				settingDifficulty = false;
				sbg.enterState(stateContainer.Game.MAINMENUSTATE);
			}
			return;

		}

		else if (settingGameMode) {

			if ((posX > 490 && posX < 850 && posY > 320 && posY < 370)) {
				multiplayer = false;
				settingDifficulty = true;
				settingGameMode = false;
			}
			else if ((posX > 500 && posX < 850 && posY > 450 && posY < 490)) {
				multiplayer = true;
				settingGameMode = false;
				connect = true;
			}
			else if (posX > 20 && posX < 136 && posY > 20 && posY < 66) {
				settingGameMode = true;
				sbg.enterState(stateContainer.Game.MAINMENUSTATE);
			}

		}


		return;

	}

	@Override
	public int getID() {
		return stateContainer.Game.PLAYSTATE;
	}

	public static Airspace getAirspace() {
		return airspace;
	}

	public void setAirspace(Airspace airspace) {
		PlayState.airspace = airspace;
	}

	public static boolean isMultiplayer() {
		return multiplayer;
	}

	public static void sendFlight(Flight flight, double newX, double newY){
		remote.sendFlight(userID, flight, newX, newY);
	}

	public static boolean isIncomingFlight(){
		return match.isIncomingAircraft();
	}

	public static String getFlightName(){
		return match.getFlightName();
	}

	public static int getFlightHeading(){
		return match.getFlightHeading();
	}

	public static int getFlightAltitude(){
		return match.getFlightAltitude();
	}

	public static int getFlightX(){
		return match.getFlightX();
	}

	public static int getFlightY(){
		return match.getFlightY();
	}

	public static void setLost(){
		remote.setLost(userID);
	}

	public String extPlayerScoreString(int playerNumber, int playerScore){
		if(playerNumber == 2)
			return (player2Name+" = "+playerScore);
		if(playerNumber == 3)
			return (player3Name+" = "+playerScore);
		else{
			assert (playerNumber == 4);
			return (player4Name+" = "+playerScore);
		}

	}

	public void prepareAirspace(Airspace airspace, int difficulty){
		if (difficulty == 1){
			airspace.setDifficultyValueOfGame(1);
			airspace.getControls().setDifficultyValueOfGame(
					Controls.EASY);
		}
		else if (difficulty == 2){
			airspace.setDifficultyValueOfGame(2);
			airspace.getControls().setDifficultyValueOfGame(
					Controls.NORMAL);
		}
		else{
			airspace.setDifficultyValueOfGame(3);
			airspace.getControls().setDifficultyValueOfGame(
					Controls.HARD);
		}
		airspace.createAndSetSeparationRules();
		this.iterator = 0; //Resets iterator to 0 so that the details of the match could be received on the first frame
	}

	public static Match getMatch() {
		return match;
	}
}