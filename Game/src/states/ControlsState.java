package states;

import java.awt.Font;
import java.io.InputStream;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.loading.LoadingList;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.ResourceLoader;

import util.DeferredFile;

/*
 * Instructions for playing the game
 */
public class ControlsState extends BasicGameState {

	private static Image nextPageButton, previousPageButton, nextPageHover, previousPageHover, menuButton, menuHover,
	controlsBackground, leftSide, controls1, exampleFlight;
	
	private StateBasedGame sbg;

	private int pageNumber;
	private static TrueTypeFont largeFont, medFont, smallFont;
	private static Color normWhite;
	public ControlsState(int state) {

	}

	@Override
	public void init(GameContainer gc, StateBasedGame sbg)
			throws SlickException {

		this.pageNumber = 1;
		this.sbg = sbg;

		{

			LoadingList loading = LoadingList.get();

			normWhite = new Color(255,255,255);

			loading.add(new DeferredFile("res/a_love_of_thunder_font/A Love of Thunder.ttf") {
				public void loadFile(String filename) {
					InputStream inputStream = ResourceLoader
							.getResourceAsStream(filename);
					try {
						Font awtFont = Font.createFont(Font.TRUETYPE_FONT,
								inputStream);
						largeFont = new TrueTypeFont(awtFont.deriveFont(28f), true);
						medFont = new TrueTypeFont(awtFont.deriveFont(24f), true);
						smallFont = new TrueTypeFont(awtFont.deriveFont(18f), true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			loading.add(new DeferredFile("res/graphics/Menu/new/controlsBackground.png") {
				public void loadFile(String filename) throws SlickException {
					controlsBackground = new Image(filename);
				}
			});


			loading.add(new DeferredFile("res/graphics/Menu/new/next_page.png") {
				public void loadFile(String filename) throws SlickException {
					nextPageButton = new Image(filename);
				}
			});

			loading.add(new DeferredFile(
					"res/graphics/Menu/new/next_page_hover.png") {
				public void loadFile(String filename) throws SlickException {
					nextPageHover = new Image(filename);
				}
			});

			loading.add(new DeferredFile(
					"res/graphics/Menu/new/previous_page.png") {
				public void loadFile(String filename) throws SlickException {
					previousPageButton = new Image(filename);
				}
			});

			loading.add(new DeferredFile(
					"res/graphics/Menu/new/previous_page_hover.png") {
				public void loadFile(String filename) throws SlickException {
					previousPageHover = new Image(filename);
				}
			});

			loading.add(new DeferredFile("res/graphics/new/left_bar_example.png") {
				public void loadFile(String filename) throws SlickException {
					leftSide = new Image(filename);
				}
			});
			
			loading.add(new DeferredFile("res/graphics/new/flight_example.png") {
				public void loadFile(String filename) throws SlickException {
					exampleFlight = new Image(filename);
				}
			});

			loading.add(new DeferredFile(
					"res/graphics/Menu/new/controls1.png") {
				public void loadFile(String filename) throws SlickException {
					controls1 = new Image(filename);
				}
			});
			

			loading.add(new DeferredFile(
					"res/graphics/Menu/new/menu_button.png") {
				public void loadFile(String filename) throws SlickException {
					menuButton = new Image(filename);
				}
			});

			loading.add(new DeferredFile("res/graphics/Menu/new/menu_hover.png") {
				public void loadFile(String filename) throws SlickException {
					menuHover = new Image(filename);
				}
			});
		}

	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)
			throws SlickException {

		int posX = Mouse.getX(),
				// Fixing posY to reflect graphics coords
				posY = stateContainer.Game.MAXIMUMHEIGHT - Mouse.getY();

		//controlsBackground.draw(0, 0);

		switch (pageNumber) {

		case 1:

			controls1.draw(0,0);

			if (posX > 1030 && posX < 1193 && posY > 535 && posY < 570) {
				nextPageHover.draw(1030, 535);
			}
			else {
				nextPageButton.draw(1030, 535);
			}

			if (posX > 20 && posX < 136 && posY > 20 && posY < 66) {
				menuHover.draw(20, 20);
			}
			else {
				menuButton.draw(20, 20);
			}


			break;


		case 2:

			controlsBackground.draw(0, 0);
			leftSide.draw(0,0);

			smallFont.drawString(550, 100, "The Hub", normWhite);
			
			largeFont.drawString(170, 150, "These are your active flights", normWhite);
			smallFont.drawString(170, 190, "The one with the border, G-FHJJ, is the selected flight", normWhite);
			
			largeFont.drawString(170, 350, "This is information about flight G-FHJJ", normWhite);
			smallFont.drawString(170, 435, "Flight plan - The list of waypoints your flight should pass through", normWhite);
			smallFont.drawString(170, 470, "Altitude - The height your flight is flying at", normWhite);
			smallFont.drawString(170, 493, "Current heading - The direction your flight is facing", normWhite);

			
			if (posX > 180 && posX < 391 && posY > 535 && posY < 570) {
				previousPageHover.draw(180, 535);
			}
			else
				previousPageButton.draw(180, 535);

			if (posX > 1030 && posX < 1193 && posY > 535 && posY < 570) {
				nextPageHover.draw(1030, 535);
			}
			else {
				nextPageButton.draw(1030, 535);
			}

			if (posX > 1050 && posX < 1166 && posY > 20 && posY < 66) {
				menuHover.draw(1050, 20);
			}
			else {
				menuButton.draw(1050, 20);
			}

			break;

		case 3:

			controlsBackground.draw(0, 0);
			exampleFlight.draw(800, 150);
			
			smallFont.drawString(500, 100, "The Radial Menu", normWhite);


			largeFont.drawString(200, 145, "This is used to control your flights.", normWhite);
			smallFont.drawString(200, 180,"Slide the circular slider to change the heading", normWhite);
			smallFont.drawString(200, 205,"Slide the horizontal slider to change the speed", normWhite);
			smallFont.drawString(200, 230,"Slide the vertical slider to change the altitude", normWhite);
			
			largeFont.drawString(200, 350, "Landing", normWhite);
			smallFont.drawString(200, 385,"To land a flight, direct it up the runway towards the exit point and press the LAND button", normWhite);


			if (posX > 30 && posX < 241 && posY > 535 && posY < 570) {
				previousPageHover.draw(30, 535);
			}
			else
				previousPageButton.draw(30, 535);


			if (posX > 1030 && posX < 1193 && posY > 535 && posY < 570) {
				nextPageHover.draw(1030, 535);
			}
			else {
				nextPageButton.draw(1030, 535);
			}

			if (posX > 20 && posX < 136 && posY > 20 && posY < 66) {
				menuHover.draw(20, 20);
			}
			else {
				menuButton.draw(20, 20);
			}

			break;


		case 4:

			controlsBackground.draw(0, 0);

			smallFont.drawString(500, 100, "Multiplayer Mode", normWhite);

			//TODO
			medFont.drawString(170, 150,"Any flight that leaves your airspace enters the airspace of another player.", normWhite);
			medFont.drawString(170, 200,"However, if the flight leaves without following it's flight plan,", normWhite);
			medFont.drawString(180, 225,"the other player can score more points from it. ", normWhite);
			
			medFont.drawString(170, 285,"Only a limited number of flights are generated by each airspace.", normWhite);
			medFont.drawString(170, 315,"The number remaining is shown in at the top of The Hub in the form:", normWhite);
			smallFont.drawString(230, 350, "Number remaining in your airspace / Number remaining in all airspaces", normWhite);
			
			medFont.drawString(170, 420,"Be quick! Once all the flights have been generated, there's only 1 minute left!", normWhite);

			if (posX > 30 && posX < 241 && posY > 535 && posY < 570) {
				previousPageHover.draw(30, 535);
			}
			else
				previousPageButton.draw(30, 535);
			
			if (posX > 20 && posX < 136 && posY > 20 && posY < 66) {
				menuHover.draw(20, 20);
			}
			else {
				menuButton.draw(20, 20);
			}

		}
		// back [to menu] button

	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta)
			throws SlickException {		
	}
	
	@Override
	/*
	 * Called when a mouse "click" event occurs - i.e. when, the mouse button is "pressed"
	 * and "released" in quick succession; AFTER the mouse has been released.
	 * 
	 * @see org.newdawn.slick.state.BasicGameState#mouseClicked(int, int, int, int)
	 */
	public void mouseClicked(int button, int posX, int posY, int clickCount) {
		
		switch (pageNumber) {

		case 1:
			
			// click to page 2
			if (posX > 1030 && posX < 1193 && posY > 535 && posY < 570) {
				pageNumber = 2;
			}
			// click on back [to menu] button
			else if (posX > 20 && posX < 136 && posY > 20 && posY < 66) {
				pageNumber = 1;
				sbg.enterState(stateContainer.Game.MAINMENUSTATE);
			}
		
			break;
			
		case 2:
		
			// click to page 1
			if (posX > 180 && posX < 391 && posY > 535 && posY < 570) {
				pageNumber = 1;
			}
			// click to page 3
			else if (posX > 1030 && posX < 1193 && posY > 535 && posY < 570) {
				pageNumber = 3;
			}
			// click on back [to menu] button
			else if (posX > 1050 && posX < 1166 && posY > 20 && posY < 66) {
				pageNumber = 1;
				sbg.enterState(stateContainer.Game.MAINMENUSTATE);
			}
		
			break;
		
		case 3:
					
			// click to page 2
			if (posX > 30 && posX < 241 && posY > 535 && posY < 570) {
				pageNumber = 2;
			}
			// click to page 4
			else if (posX > 1030 && posX < 1193 && posY > 535 && posY < 570) {
				pageNumber = 4;
			}
			// click on back [to menu] button
			else if (posX > 20 && posX < 136 && posY > 20 && posY < 66) {
				pageNumber = 1;
				sbg.enterState(stateContainer.Game.MAINMENUSTATE);
			}
			
			break;
			
		case 4:
			
			// click to page 3
			if (posX > 30 && posX < 241 && posY > 535 && posY < 570) {
				pageNumber = 3;
			}
			// click on back [to menu] button
			else if (posX > 20 && posX < 136 && posY > 20 && posY < 66) {
				pageNumber = 1;
				sbg.enterState(stateContainer.Game.MAINMENUSTATE);
			}
		
			break;
			
		}

	}

	public int getID() {
		return stateContainer.Game.CONTROLSSTATE;
	}

}
