package states;

import java.awt.Font;
import java.io.InputStream;

import logicClasses.Achievements;
import logicClasses.ScoreTracking;
import multiplayer.Player;
import multiplayer.Match;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.loading.LoadingList;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.ResourceLoader;

import util.DeferredFile;

public class GameOverState extends BasicGameState {

	private static Image menuButton, playAgainButton,
			menuHover, playAgainHover, gameOverBackground;
	private TrueTypeFont bigFont, normFont; 
	private Color white;
	private Achievements achievement;	
	private Match match;
	private int myScore;
	
	public GameOverState(int state) {
		achievement = new Achievements();

	}

	@Override
	public void init(GameContainer gc, StateBasedGame sbg)
			throws SlickException {
		{
			white = new Color(255,255,255);
			
			LoadingList loading = LoadingList.get();

			loading.add(new DeferredFile(
					"res/graphics/Menu/new/gameover_screen.png") {
				public void loadFile(String filename) throws SlickException {
					gameOverBackground = new Image(filename);
				}
			});

			loading.add(new DeferredFile(
					"res/graphics/Menu/new/playagain_button.png") {
				public void loadFile(String filename) throws SlickException {
					playAgainButton = new Image(filename);
				}
			});

			loading.add(new DeferredFile(
					"res/graphics/Menu/new/menu_button.png") {
				public void loadFile(String filename) throws SlickException {
					menuButton = new Image(filename);
				}
			});

			loading.add(new DeferredFile(
					"res/graphics/Menu/new/playagain_hover.png") {
				public void loadFile(String filename) throws SlickException {
					playAgainHover = new Image(filename);
				}
			});

			loading.add(new DeferredFile("res/a_love_of_thunder_font/A Love of Thunder.ttf") {
				public void loadFile(String filename) {
					InputStream inputStream = ResourceLoader
							.getResourceAsStream(filename);
					try {
						Font awtFont = Font.createFont(Font.TRUETYPE_FONT,
								inputStream);
						bigFont = new TrueTypeFont(awtFont.deriveFont(56f), true);
						normFont = new TrueTypeFont(awtFont.deriveFont(32f), true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			
			loading.add(new DeferredFile("res/graphics/Menu/new/menu_hover.png") {
				public void loadFile(String filename) throws SlickException {
					menuHover = new Image(filename);
				}
			});
		}
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)
			throws SlickException {

		gameOverBackground.draw(0, 0);

		if(PlayState.isMultiplayer() == true){
			match = PlayState.getMatch();
			myScore = ScoreTracking.getScore();
			if(match.isVictory() == true){
				bigFont.drawString(450, 100, "YOU WON!", white);
			}else if (match.isTimerOn() == true && match.getTimer() < 1){
				boolean victory = true;
				Player[] players = match.getPlayers();
				for(int i = 0; i < match.getNoOfPlayers() - 1; i++){
					if(players[i].getScore() > myScore)
						victory = false;
				}
				if(victory){
					bigFont.drawString(450, 100, "YOU WON!", white);					
				}else{
					bigFont.drawString(450, 100, "YOU LOST", white);
					PlayState.setLost();
				}
			}else{
				bigFont.drawString(450, 100, "YOU LOST", white);
				PlayState.setLost();
			}
		}

		
		int posX = Mouse.getX();
		int posY = stateContainer.Game.MAXIMUMHEIGHT - Mouse.getY();
		// Fixing posY to reflect graphics coords

		if (posX > 728 && posX < 844 && posY > 380 && posY < 426)
			menuHover.draw(728, 380);
		else
			menuButton.draw(728, 380);

		if (posX > 354 && posX < 582 && posY > 380 && posY < 424)
			playAgainHover.draw(354, 380);
		else
			playAgainButton.draw(354, 380);

		g.drawString(achievement.crashAchievement(60), 900, 30);
		g.setColor(Color.white);
		

		normFont.drawString(500, 500, "My Score: "+ScoreTracking.getScore(), white);
		
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta)
			throws SlickException {

		int posX = Mouse.getX(),
		    posY = stateContainer.Game.MAXIMUMHEIGHT - Mouse.getY();

		if (Mouse.isButtonDown(Input.MOUSE_LEFT_BUTTON)) {

			if (posX > 354 && posX < 582 && posY > 380 && posY < 424) {
				sbg.enterState(stateContainer.Game.PLAYSTATE);
			}
			else if (posX > 728 && posX < 844 && posY > 380 && posY < 426) { // 116
																		// 46
				sbg.enterState(stateContainer.Game.MAINMENUSTATE);
			}

		}

	}

	@Override
	public int getID() {
		return stateContainer.Game.GAMEOVERSTATE;
	}
}
