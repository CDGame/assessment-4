package multiplayer;

public class Match {
	private boolean isIncomingAircraft;
	private int noOfPlayers;
	private boolean timerOn;
	private int timer;
	private int isVictory;
	private String flightName;
	private int flightHeading, flightAltitude, flightX, flightY;
	private Player[] players;
	
	public Match (boolean isIncomingAircraft, int noOfPlayers, boolean timerOn, int timer, int isVictory, String flightName, int flightHeading, int flightAltitude, int flightX, int flightY, Player[] players){
		this.isIncomingAircraft = isIncomingAircraft;
		this.noOfPlayers = noOfPlayers;
		this.timerOn = timerOn;
		this.timer = timer;
		this.isVictory = isVictory;
		this.flightName = flightName;
		this.flightHeading = flightHeading;
		this.flightAltitude = flightAltitude;
		this.flightX = flightX;
		this.flightY = flightY;
		this.players = players;
	}
	
	public boolean isIncomingAircraft(){
		return this.isIncomingAircraft;
	}
	
	public int getNoOfPlayers(){
		return this.noOfPlayers;
	}
	
	public boolean isTimerOn(){
		return this.timerOn;
	}
	
	public int getTimer(){
		return this.timer;
	}

	public boolean isVictory(){
		if (this.isVictory == 1)
			return true;
		else
			return false;
	}
	
	public String getFlightName(){
		return this.flightName;
	}
	
	public int getFlightHeading(){
		return this.flightHeading;
	}
	
	public int getFlightAltitude(){
		return this.flightAltitude;
	}		

	public int getFlightX(){
		return this.flightX;
	}
	
	public int getFlightY(){
		return this.flightY;
	}
	
	public Player[] getPlayers(){
		return this.players;
	}
}
