package multiplayer;

public class Player {
	private String name;
	private int score;
	private int remainingAircrafts;
	private boolean lost;
	
	public Player (String name, int score, int remainingAircrafts, boolean lost){
		this.name = name;
		this.score = score;
		this.remainingAircrafts = remainingAircrafts;
		this.lost = lost;
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getScore(){
		return this.score;
	}
	
	public int getRemainingAircrafts(){
		return this.remainingAircrafts;
	}

	public boolean lost(){
		return this.lost;
	}

}
