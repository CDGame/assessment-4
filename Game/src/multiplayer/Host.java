package multiplayer;

public class Host {
	private int id;
	private String name;
	private int playersJoined;
	private int totalPlayers;
	
	public Host (int id, String name, int playersJoined, int totalPlayers){
		this.id = id;
		this.name = name;
		this.playersJoined = playersJoined;
		this.totalPlayers = totalPlayers;
	}
	
	public int getID(){
		return this.id;
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getPlayersJoined(){
		return this.playersJoined;
	}
	
	public int getTotalPlayers(){
		return this.totalPlayers;
	}
	
}
