package multiplayer;

import java.net.HttpURLConnection;  
import java.net.URL;  
import java.net.MalformedURLException;  
import java.io.IOException;  
import java.io.*;  
import logicClasses.Flight;

public class RemoteConnection{  
	
	BufferedReader in;
	HttpURLConnection urlConn;
	
	private void connect(String link){  
		
		try{   
		    URL url = new URL("http://sepr.wgm.lt/" + link);  
		    urlConn = (HttpURLConnection)url.openConnection(); 
		    urlConn.setUseCaches(false);  
		    System.setProperty("http.keepAlive", "false");
		    urlConn.setRequestMethod("GET");  
		    urlConn.setRequestProperty ("Content-Type","application/x-www-form-urlencoded");  
		    urlConn.setDoInput (true);  
		    in = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
			urlConn.setConnectTimeout(10);
			urlConn.setReadTimeout(10);
			//urlConn.disconnect();
		}
	    catch(MalformedURLException e){  	   
		}
	    catch(IOException e){  
		}
	    catch(Exception e){  
		    System.out.println(e.getMessage());  
		    System.out.println(e.getStackTrace());  
	    }
	}

	public int newUser(String name){
		this.connect("MutiplayerState.php?act=new_user&name=" + name);
		try{
			return Integer.parseInt(in.readLine());
		}catch(IOException e){
			return 0;
		}
		finally{
			try{
				in.close();
				urlConn.disconnect();
			}catch(IOException e){}
		}
	}

	public void newHost(int host_id, int players){
		this.connect("MutiplayerState.php?act=new_host&state=hosting&user_id=" + host_id + "&id=" + host_id + "&players=" + players);
		try{
			in.close();
			urlConn.disconnect();
		}catch(IOException e){}
	}

	public Host[] getHosts(int userID){
		this.connect("MutiplayerState.php?act=get_hosts&state=joining&user_id=" + userID);
		String inputLine;
		String[] line;
	    int number = 0, hostID, playersJoined, totalPlayers;
		Host[] hosts = new Host[7];
		try{
			while ((inputLine = in.readLine()) != null && number < 7){
				line = inputLine.split(" ");
				hostID = Integer.parseInt(line[0]);
				playersJoined = Integer.parseInt(line[2]);
				totalPlayers = Integer.parseInt(line[3]);
		    	hosts[number] = new Host(hostID, line[1], playersJoined, totalPlayers);
		    	number++;
		    }
		}catch(IOException e){
		}
		finally{
			try{
				in.close();
				urlConn.disconnect();
			}catch(IOException e){}
		}
		return hosts;
	}
	
	public boolean gameExists(int user_id){
		this.connect("MutiplayerState.php?act=game_exists&state=hosting&user_id=" + user_id);
		try{
			if (Integer.parseInt(in.readLine()) == 1)
				return true;
			else
				return false;
		}catch(IOException e){
			return false;
		}
		finally{
			try{
				in.close();
				urlConn.disconnect();
			}catch(IOException e){}
		}
	}

	public void joinGame(int host_id, int user_id){
		this.connect("MutiplayerState.php?act=join_game&state=joining&host_id=" + host_id + "&user_id=" + user_id);
		try{
			in.close();
			urlConn.disconnect();
		}catch(IOException e){}
	}

	public void createGame(int hostID){
		this.connect("MutiplayerState.php?act=create_game&host=" + hostID);
		try{
			in.close();
			urlConn.disconnect();
		}catch(IOException e){}
	}
	
	public void sendFlight(int userID, Flight flight, double newX, double newY){
		this.connect("MutiplayerState.php?act=send_flight&user_id=" + userID + "&name=" + flight.getFlightName() + "&heading=" + flight.getCurrentHeading() + "&altitude=" + flight.getAltitude() + "&x=" + newX + "&y=" + newY);
		try{
			in.close();
			urlConn.disconnect();
		}catch(IOException e){}
	}


	public Match getMatchInfo(int userID, int myScore, int aircraftsRemaining){
		this.connect("MutiplayerState.php?act=match_info&state=playing&user_id=" + userID + "&score=" + myScore + "&aircrafts=" + aircraftsRemaining);
		String inputLine, playerName, flightName = null;
		String[] line;
	    int incomingAircraft = 0, noOfPlayers = 0, timerOn = 0, timer = 0, isVictory = 0;
	    int flightHeading = 0, flightAltitude = 0, flightX = 0, flightY = 0;
		Player[] players = new Player[4];
		int number = 0, score, remainingAircrafts;
		boolean isIncomingAircraft = false, isTimerOn = false, lost;
		Match match;
		try{
			inputLine = in.readLine();
			line = inputLine.split(" ");
			incomingAircraft = Integer.parseInt(line[0]);
			if(incomingAircraft == 1)
				isIncomingAircraft = true;
			else
				isIncomingAircraft = false;
			noOfPlayers = Integer.parseInt(line[1]);
			timerOn = Integer.parseInt(line[2]);
			timer = Integer.parseInt(line[3]);
			isVictory = Integer.parseInt(line[4]);
			if(timerOn == 1)
				isTimerOn = true;
			else
				isTimerOn = false;
			if(isIncomingAircraft){
				inputLine = in.readLine();
				line = inputLine.split(" ");
				flightName = line[0];
				flightHeading = Integer.parseInt(line[1]);
				flightAltitude = Integer.parseInt(line[2]);
				flightX = Integer.parseInt(line[3]);
				flightY = Integer.parseInt(line[4]);
			}
			while ((inputLine = in.readLine()) != null){
				line = inputLine.split(" ");
				playerName = line[0];
				score = Integer.parseInt(line[1]);
				remainingAircrafts = Integer.parseInt(line[2]);
				if(Integer.parseInt(line[3]) == 1){
					lost = true;
				}
				else
					lost = false;
				players[number] = new Player(playerName, score, remainingAircrafts, lost);
		    	number++;
		    }
			match = new Match(isIncomingAircraft, noOfPlayers, isTimerOn, timer, isVictory, flightName, flightHeading, flightAltitude, flightX, flightY, players);
		}catch(IOException e){
			match = new Match(false, 0, false, 0, 0, null, 0, 0, 0, 0, null);
		}
		finally{
			try{
				in.close();
				urlConn.disconnect();
			}catch(IOException e){}
		}
		return match;
	}

	public void setLost(int userID){
		this.connect("MutiplayerState.php?act=set_lost&user_id=" + userID);
		try{
			in.close();
			urlConn.disconnect();
		}catch(IOException e){}
	}

	public void startTimer(int userID){
		this.connect("MutiplayerState.php?act=start_timer&user_id=" + userID);
		try{
			in.close();
			urlConn.disconnect();
		}catch(IOException e){}
	}
	
}  