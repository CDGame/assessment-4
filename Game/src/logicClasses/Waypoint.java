package logicClasses;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.loading.LoadingList;

import util.DeferredFile;

public class Waypoint extends Point {

	static Image nextWaypointImage, waypointImage;

	public Waypoint(double xcoord, double ycoord, String name) {
		super(xcoord, ycoord, name);
	}

	/**
	 * Initialises the variables and loads resources required for rendering
	 * (sets waypointImage and nextWaypointImage)
	 * 
	 * @param gc Slick2d game container
	 * @throws SlickException Slick2d exception handler
	 */
	public void init(GameContainer gc) throws SlickException {
		LoadingList loading = LoadingList.get();
		if (waypointImage == null) {
			loading.add(new DeferredFile("res/graphics/waypoint.png") {
				public void loadFile(String filename) throws SlickException {
					waypointImage = new Image(filename);
				}
			});
		}
		if (nextWaypointImage == null) {
			loading.add(new DeferredFile("res/graphics/waypoint_next.png") {
				public void loadFile(String filename) throws SlickException {
					nextWaypointImage = new Image(filename);
				}
			});
		}
	}

	/**
	 * Render the graphics for a Waypoint. A special image is used for
	 * the next Waypoint in a selected Flight's FlightPlan (if any)
	 * 
	 * @param g slick2d graphics object
	 * @param airspace object
	 * @throws SlickException Slick2d exception handler
	 */
	public void render(Graphics g, Airspace airspace) throws SlickException {
		// background circle
		Image image;
		if (
		    airspace.getControls().getSelectedFlight() != null
		    &&
		    (airspace.getControls().getSelectedFlight().getFlightPlan().getCurrentRoute().indexOf(this) == 0)
		    ) {
			image = nextWaypointImage;
		}
		else {
			image = waypointImage;
		}
		image.draw((int) x - 14, (int) y - 14, 30, 30);
		
		// text (single capital letter) on circle
		g.setColor(Color.black);
		g.drawString(pointRef, (int) x - 3, (int) y - 9);
	}

}
