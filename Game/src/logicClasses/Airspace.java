package logicClasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import states.PlayState;

public class Airspace {

	private static int maximumNumberOfFlightsInAirspace;
	private int numberOfGameLoopsSinceLastFlightAdded, numberOfGameLoops,
			numberOfGameLoopsWhenDifficultyIncreases,
			randomNumberForFlightGeneration;
	private static int numberOfFlightsGenerated;							// Increases throughout game
	private static int maxNumberFlightsGeneratedMultiplayer;				// Constant
	private static List<Flight> listOfFlightsInAirspace;
	private List<Waypoint> listOfWayppoints;
	private List<EntryPoint> listOfEntryPoints;
	private List<ExitPoint> listOfExitPoints;
	private SeparationRules separationRules;
	private Airport airport1, airport2;
	private int difficultyValueOfGame;
	private Controls controls;
	private static ScoreTracking score;

	// CONSTRUCTOR
	public Airspace() {
		maximumNumberOfFlightsInAirspace = 10;
		listOfFlightsInAirspace = new ArrayList<Flight>();
		this.listOfWayppoints = new ArrayList<Waypoint>();
		this.listOfEntryPoints = new ArrayList<EntryPoint>();
		this.listOfExitPoints = new ArrayList<ExitPoint>();
		this.airport1 = new Airport(
				(stateContainer.Game.MAXIMUMWIDTH - 150)/2 + 200, stateContainer.Game.MAXIMUMHEIGHT/2 - 50);
		this.airport2 = new Airport(
				(stateContainer.Game.MAXIMUMWIDTH - 150)/2 - 230, stateContainer.Game.MAXIMUMHEIGHT/2 + 50);
		this.numberOfGameLoopsSinceLastFlightAdded = 0; 
		// Stores how many loops since the last flight was spawned before another flight can enter
		
		this.numberOfGameLoops = 0; 
		// Stores how many loops there have been in total
		
		this.numberOfGameLoopsWhenDifficultyIncreases = 3600; 
		// this is how many loops until planes come more quickly, difficulty increase once a minute
		
		this.randomNumberForFlightGeneration = 500;
		this.controls = new Controls();
		this.difficultyValueOfGame = 0; 
		// This value will be changed when the user selects a difficulty in the playstate
		
		numberOfFlightsGenerated = 0;
		maxNumberFlightsGeneratedMultiplayer = 15;
		
		Airspace.score = new ScoreTracking();
		
	}

	// METHODS

	/**
	 * resetAirspace: Reset all of the attributes in airspace back to default
	 */

	public void resetAirspace() {

		listOfFlightsInAirspace = new ArrayList<Flight>();

		this.numberOfGameLoopsSinceLastFlightAdded = 0;
		this.numberOfGameLoops = 0;
		this.numberOfGameLoopsWhenDifficultyIncreases = 3600;
		this.separationRules.setGameOverViolation(false); 
		// Prevents user immediately entering game over state upon replay
		
		this.controls.setSelectedFlight(null); 
		// Prevents information about flight from previous game being displayed
	}

	/**
	 * createAndSetSeperationRules: Create and set the separation rules for the
	 * airpsace based on the difficulty value of the game
	 */

	public void createAndSetSeparationRules() {
		this.separationRules = new SeparationRules(difficultyValueOfGame);
	}

	/**
	 * newWaypoint: Add a new waypoint to the list of waypoints in the airspace
	 * 
	 * @param x
	 *            The x coordinate of the waypoint
	 * @param y
	 *            The y coordinate of the waypoint
	 * @param name
	 *            The name used to reference the waypoint
	 */

	public boolean newWaypoint(int x, int y, String name) {
		if (x < 1250 && x > 150 && y < 650 && y > -50) {
			// x and y must be within these bounds to be within screen space

			Waypoint tmpWp = new Waypoint(x, y, name);

			if (this.addWaypoint(tmpWp)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * newExitPoint: Add a new exitpoint to the list in the airspace
	 * 
	 * @param x
	 *            The x coordinate of the exitpoint
	 * @param y
	 *            The y coordinate of the exitpoint
	 * @param name
	 *            The name used to reference the exitpoint
	 */

	public boolean newExitPoint(int x, int y, String name) {
		if (x < 1250 && x > 100 && y < 650 && y > -50) {
			// x and y must be within these bounds to be within screen space

			ExitPoint tmpEp = new ExitPoint(x, y, name);

			tmpEp.setPointRef("EXP" + name);
			if (this.addExitPoint(tmpEp)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * newEntryPoint: Add a new entrypoint to the the list in the airspace
	 * 
	 * @param x
	 *            The x coordinate of the entry point
	 * @param y
	 *            The y coordinate of the entry point
	 */

	public boolean newEntryPoint(int x, int y) {
		if (x < 1250 && x > 100 && y < 650 && y > -50) {

			EntryPoint tmpEp = new EntryPoint(x, y);

			if (this.addEntryPoint(tmpEp)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * newFlight: Add a new flight to the list of flights in the airspace if it
	 * has been long enough since the last flight was added and if random number
	 * satisfies condition The flight is also given a name
	 * 
	 * @param gc
	 *            Game container required by Slick2d
	 * @throws SlickException
	 */

	public boolean newFlight(GameContainer gc) throws SlickException {

		//Checks whether an aircraft is coming from another airspace
		if(PlayState.isMultiplayer() && PlayState.isIncomingFlight()){
			Flight newFlight = new Flight(this);
			newFlight.setFlightName(PlayState.getFlightName());
			newFlight.setCurrentAltitude(PlayState.getFlightAltitude());
			newFlight.setTargetAltitude(PlayState.getFlightAltitude());
			newFlight.setCurrentHeading(PlayState.getFlightHeading());
			newFlight.setTargetHeading(PlayState.getFlightHeading());
			newFlight.setFromOtherAirspace(true);
			newFlight.setX(PlayState.getFlightX());
			newFlight.setY(PlayState.getFlightY());
			if (addFlight(newFlight)) {
				this.numberOfGameLoopsSinceLastFlightAdded = 0;
				listOfFlightsInAirspace.get(
						listOfFlightsInAirspace.size() - 1).init(
						gc);
			}
		}
		
		if (listOfFlightsInAirspace.size() < maximumNumberOfFlightsInAirspace) {

			if ((this.numberOfGameLoopsSinceLastFlightAdded >= 850 || listOfFlightsInAirspace
					.isEmpty())) {

				Random rand = new Random();
				int checkNumber;

				if (listOfFlightsInAirspace.isEmpty()) {
					checkNumber = rand.nextInt(100); 
					// A random number (checkNumber) is generated in the range [0, 100)
				}

				else {
					checkNumber = rand
							.nextInt(this.randomNumberForFlightGeneration); 
					// A random number (checkNumber) is generated in range [0, randomNumberForFlightGeneration)
				}

				/*
				 * The random number is generated in the range [0, 100) if the
				 * airspace is empty, as this increases the likelihood of a
				 * value of 1 being returned, and therefore a flight being
				 * generated; this stops the user having to potentially wait a
				 * long period of time for a flight to be generated. If the
				 * airspace is not empty, the random number generated is in the
				 * range [0, randomNumberForFlight Generation) which is > 100.
				 * This decreases the likelihood of a flight being generated.
				 */

				if (checkNumber == 1) {

					Flight tempFlight = new Flight(this);
					tempFlight.setFlightName(this.generateFlightName());
					tempFlight.setTargetAltitude(tempFlight.getAltitude());
					double heading;
					if (tempFlight.getFlightPlan().getEntryPoint().isRunway()) {
						heading = airport1.getRunwayHeading();
					} else {
						heading = tempFlight.calculateHeadingToFirstWaypoint(
								tempFlight.getFlightPlan().getPointByIndex(0)
										.getX(), tempFlight.getFlightPlan()
										.getPointByIndex(0).getY());
					}
					tempFlight.setTargetHeading(heading);
					tempFlight.setCurrentHeading(heading);
					if (addFlight(tempFlight)) {
						this.numberOfGameLoopsSinceLastFlightAdded = 0;
						listOfFlightsInAirspace.get(
								listOfFlightsInAirspace.size() - 1).init(
								gc);
						return true;
					}

				}
			}
		}
		return false;
	}

	/**
	 * generateFlightName: Generate a random name for a flight, based on UK
	 * flight tail numbers
	 * 
	 * @return Returns a random string that can be used to identify a flight.
	 */

	public String generateFlightName() {
		String name = "G-";
		Random rand = new Random();
		for (int i = 0; i < 4; i++) {
			int thisChar = rand.nextInt(10) + 65; // Generates int in range [65,74]
			name += (char) thisChar; // Generate corresponding ascii character for int
		}
		return name;
	}

	/**
	 * checkIfFlightHasLeftAirspace: Check if a flight is outside the area of
	 * the game, and if it is removed the object so it is not using unnecessary
	 * resources.
	 * 
	 * @param flight
	 *            The flight being checked.
	 */

	public boolean checkIfFlightHasLeftAirspace(Flight flight) {

		if (flight.getX() > 1250 || flight.getX() < 100 || flight.getY() > 650
				|| flight.getY() < -50) { // x and y must be within these bounds to be within screen space
			return true;
		} else {
			return false;
		}

	}

	/**
	 * increaseDifficulty
	 */

	public void increaseDifficulty() {
		this.numberOfGameLoopsWhenDifficultyIncreases += 3600;
		if (this.randomNumberForFlightGeneration - 50 > 0) {
			this.randomNumberForFlightGeneration -= 50;
		}
	}
	
	// INIT, RENDER, UPDATE

	/**
	 * init: Initialises all the resources required for the airspace class, and
	 * any other classes that are rendered within it
	 * 
	 * @param gc
	 *            GameContainer
	 * @throws SlickException
	 */

	public void init(GameContainer gc) throws SlickException {

		this.controls.init(gc);
		this.airport1.init(gc);
		this.airport2.init(gc);

		for (int i = 0; i < this.listOfWayppoints.size(); i++) { // Initialising waypoints
			this.listOfWayppoints.get(i).init(gc);
		}

		for (int i = 0; i < this.listOfExitPoints.size(); i++) { // Initialising exit points
			this.listOfExitPoints.get(i).init(gc);
		}

		for (int i = 0; i < this.listOfEntryPoints.size(); i++) { // Initialising entry point
			this.listOfEntryPoints.get(i).init(gc);
		}

	}

	/**
	 * update: Update all logic in the airspace class
	 * 
	 * @param gc
	 *            GameContainer
	 */

	public void update(GameContainer gc) {

		
		this.numberOfGameLoopsSinceLastFlightAdded++;
		this.numberOfGameLoops++;
		if (this.numberOfGameLoops >= this.numberOfGameLoopsWhenDifficultyIncreases) {
			this.increaseDifficulty();

		}

		for (int i = 0; i < listOfFlightsInAirspace.size(); i++) {
			Flight flight = listOfFlightsInAirspace.get(i);
			flight.update(score);
			
			if (flight.getFlightPlan()
					.getCurrentRoute().size() == 0) {
				this.removeSpecificFlight(i);
			} 
			else if (this.checkIfFlightHasLeftAirspace(flight)) {
				score.reduceScoreOnFlightLost();
				
				if (PlayState.isMultiplayer())
				{
					double currentX = flight.getX();
					double currentY = flight.getY();
					double newX, newY;
					
					if (currentX > 1250)
						newX = currentX - 1150;
					else if (currentX < 100)
						newX = 1250 - (100 - currentX);
					else
						newX = flight.getX();
					
					if (currentY > 650)
						newY = currentY - 700;
					else if (currentY < -50)
						newY = currentY + 650;
					else
						newY = currentY;
					
					PlayState.sendFlight(flight, newX, newY);
				}
				this.removeSpecificFlight(i);
			}

		}

		this.separationRules.update(this);
		this.controls.update(gc, this);
	}

	public static ScoreTracking getScore() {
		return score;
	}

	/**
	 * render: Render all of the graphics in the airspace
	 * 
	 * @param g
	 *            Graphics
	 * @param gc
	 *            GameContainer
	 * 
	 * @throws SlickException
	 */
	public void render(Graphics g, GameContainer gc) throws SlickException {

		this.airport1.render(g, gc);
		this.airport2.render(g, gc);

		for (Waypoint w : listOfWayppoints) { // Draws waypoints
			w.render(g, this);
		}
		for (ExitPoint e : listOfExitPoints) { // Draws exit points
			e.render(g, this);
		}
		for (EntryPoint e : listOfEntryPoints) { // Draws entry points
			e.render(g);
		}
		for (Flight f : listOfFlightsInAirspace) { // Draws flights in airspace
			f.render(g, gc);
		}

		separationRules.render(g, gc, this);
		controls.render(gc, g);
	}

	// MUTATORS AND ACCESSORS

	public int getMaxNumberOfFlights() {
		return maximumNumberOfFlightsInAirspace;
	}

	public List<Flight> getListOfFlights() {
		return listOfFlightsInAirspace;
	}

	public List<Waypoint> getListOfWaypoints() {
		return this.listOfWayppoints;
	}

	public List<EntryPoint> getListOfEntryPoints() {
		return this.listOfEntryPoints;
	}

	public List<ExitPoint> getListOfExitPoints() {
		return this.listOfExitPoints;
	}

	public void setMaxNumberOfFlights(int maxNumberOfFlights) {
		maximumNumberOfFlightsInAirspace = maxNumberOfFlights;
	}

	public boolean addWaypoint(Waypoint waypoint) {
		if (this.listOfWayppoints.contains(waypoint)) {
			return false;
		} else {
			this.listOfWayppoints.add(waypoint);
			return true;
		}
	}

	public boolean addEntryPoint(EntryPoint entrypoint) {
		if (this.listOfEntryPoints.contains(entrypoint)) {
			return false;
		} else {
			this.listOfEntryPoints.add(entrypoint);
			return true;
		}
	}

	public boolean addExitPoint(ExitPoint exitpoint) {
		if (this.listOfExitPoints.contains(exitpoint)) {
			return false;
		} else {
			this.listOfExitPoints.add(exitpoint);
			return true;
		}
	}

	public static boolean addFlight(Flight flight) {

		// Checks if flight is from another airspace (multiplayer) and has already been added to this airspace
		if (flight.isFromOtherAirspace())
		{
			if (listOfFlightsInAirspace.contains(flight))
				return false;
			
			else
			{
				flight.flightPlan = new FlightPlan(PlayState.getAirspace(), flight); 
				listOfFlightsInAirspace.add(flight);
				return true;
			}
		}
		
		else
		{
			// Checks whether the flight was already added before, and if it won't
			// pass the maximum number of flights allowed
			if ((listOfFlightsInAirspace.contains(flight))
					|| (listOfFlightsInAirspace.size() > maximumNumberOfFlightsInAirspace - 1)) 
				return false;
			 
			
			if (PlayState.isMultiplayer() && !flight.isFromOtherAirspace() && numberOfFlightsGenerated >= maxNumberFlightsGeneratedMultiplayer)
				return false;
			
			else {
				for (Flight a : listOfFlightsInAirspace) {
					if (a.isGrounded() && flight.getFlightPlan().getEntryPoint().isRunway()) {
						return false;
					}
				}
				if (PlayState.isMultiplayer())
					numberOfFlightsGenerated += 1;
				
				listOfFlightsInAirspace.add(flight);
				return true;
			}
		}
	}

	public void removeSpecificFlight(int flight) {
		listOfFlightsInAirspace.remove(flight);

		// If flight was selected, de-select it
		if (!(listOfFlightsInAirspace.contains(this.controls
				.getSelectedFlight()))) {
			this.controls.setSelectedFlight(null);

		}
	}

	public void removeWaypoint(Waypoint waypoint) {
		this.listOfWayppoints.remove(waypoint);
	}

	public void removeEntryPoint(EntryPoint entrypoint) {
		this.listOfEntryPoints.remove(entrypoint);
	}

	public void removeExitPoint(ExitPoint exitpoint) {
		this.listOfExitPoints.remove(exitpoint);
	}

	public SeparationRules getSeparationRules() {
		return this.separationRules;
	}

	public void setListOfEntryPoints(List<EntryPoint> listOfEntryPoints) {
		this.listOfEntryPoints = listOfEntryPoints;
	}

	public Controls getControls() {
		return this.controls;
	}

	public void setDifficultyValueOfGame(int i) {
		this.difficultyValueOfGame = i;
	}

	public int getDifficultyValueOfGame() {
		return this.difficultyValueOfGame;
	}

	public int getNumberOfGameLoops() {
		return this.numberOfGameLoops;
	}

	public int getNumberOfGameLoopsWhenDifficultyIncreases() {
		return this.numberOfGameLoopsWhenDifficultyIncreases;
	}

	public Airport getAirport1() {
		return airport1;
	}
	
	public Airport getAirport2() {
		return airport2;
	}

	public static int aircraftsRemaining(){
		return maxNumberFlightsGeneratedMultiplayer - numberOfFlightsGenerated;
	}
	
	@Override
	public String toString() {
		String s = "Airspace: " + airport1.toString() + " , " + airport2.toString();
		return s;
	}
	
	public static int getFlightsRemainingToGenerate() {
		return (maxNumberFlightsGeneratedMultiplayer - numberOfFlightsGenerated);
	}
	public static int getMaxNumberFlightsGeneratedMultiplayer() {
		return maxNumberFlightsGeneratedMultiplayer;
	}
}
