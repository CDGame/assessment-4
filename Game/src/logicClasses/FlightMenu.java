package logicClasses;

import java.awt.Font;
import java.awt.geom.Point2D;

import static java.lang.Math.PI;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.MouseListener;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;

/*
 * The three moving slider controls (for speed, altitude and heading)
 * that appear when an aircraft is clicked on.
 */
public class FlightMenu implements MouseListener {
	private static Image // base images
			sliderBase, sliderRingBase,
			sliderIndicator, sliderIndicatorSelect,
			button, buttonSelect;

	private int // graphics data
			altitudeSize = 100, speedSize = 100, headingSize = 120, // slider base lengths
			sliderWidth = 12, indicatorSize = 20, // track and indicator sizes
			buttonWidth = 55, buttonHeight = 25, // buttonSizes
			spacingSize = 6; // spacing between components

	private Image // scaled instance copies
			altitudeBase,
			speedBase, headingBase, aIndicator, aIndicatorSelect,
			aButton,
			aButtonSelect;
	
	private TrueTypeFont labelFont = new TrueTypeFont(new Font(Font.SANS_SERIF,
			Font.BOLD, 10), false), buttonFont = new TrueTypeFont(new Font(
			Font.SANS_SERIF, Font.BOLD, 11), false);
	
	private Color labelColor = Color.white, // slider labels
			buttonColor = Color.white, // button labels
			markerColor = Color.red; // current position markers

	private Point2D.Float // cached graphics position data
			altPos,
			speedPos, headingPos, cmdPos, abortPos,
			altMarkerPos, speedMarkerPos,
			altIndicatorPos, speedIndicatorPos, headingIndicatorPos;

	private Input input;
	private Flight flight; // bound Flight

	private int mode = NONE; // active subcomponent
	private static final int NONE = 0, ALTITUDE = 1, SPEED = 2, HEADING = 3,
			CMD = 4, ABORT = 5;
	
	/*
	 * normalised indicator positions, range 0-1
	 */
	private double altIndicator, speedIndicator;

	/*
	 * heading in radians
	 */
	private double headingIndicator; // heading in radians

	public FlightMenu() {
		// initialise objects, initialise layout
		altPos = new Point2D.Float();
		speedPos = new Point2D.Float();
		headingPos = new Point2D.Float();
		cmdPos = new Point2D.Float();
		abortPos = new Point2D.Float();
		altMarkerPos = new Point2D.Float();
		speedMarkerPos = new Point2D.Float();
		altIndicatorPos = new Point2D.Float();
		speedIndicatorPos = new Point2D.Float();
		headingIndicatorPos = new Point2D.Float();
		position();
	}

	public void init() throws SlickException {
		// load images if needed
		if (sliderBase == null) {
			sliderBase = new Image("res/graphics/FlightMenu/rectangle_slider.png");
		}
		if (sliderRingBase == null) {
			sliderRingBase = new Image("res/graphics/FlightMenu/circle_slider.png");
		}
		if (sliderIndicator == null) {
			sliderIndicator = new Image("res/graphics/FlightMenu/slider_element.png");
		}
		if (button == null) {
			button = new Image("res/graphics/FlightMenu/button_bg.png");
		}
		// {!} unimplemented graphics; duplicating existing:
		sliderIndicatorSelect = sliderIndicator;
		buttonSelect = button;

	}

	/*
	 * Render the whole FlightMenu (one per selected aircraft)
	 */
	public void render(Graphics g, GameContainer gc) throws SlickException {
		
		if (flight != null) {
			// create scaled copies of images if invalidated
			if (altitudeBase == null) {
				altitudeBase = sliderBase.getScaledCopy(altitudeSize, sliderWidth);
				altitudeBase.setCenterOfRotation(0, 0);
				altitudeBase.setRotation(90); // {!} will leave image positioned
											// sliderWidth to the left
			}
			if (speedBase == null) {
				speedBase = sliderBase.getScaledCopy(speedSize, sliderWidth);
			}
			if (headingBase == null) {
				headingBase = sliderRingBase.getScaledCopy(headingSize, headingSize);
			}
			if (aIndicator == null) {
				aIndicator = sliderIndicator.getScaledCopy(indicatorSize, indicatorSize);
			}
			if (aIndicatorSelect == null) {
				aIndicatorSelect = sliderIndicatorSelect.getScaledCopy(indicatorSize, indicatorSize);
			}
			if (aButton == null) {
				aButton = button.getScaledCopy(buttonWidth, buttonHeight);
			}
			if (aButtonSelect == null) {
				aButtonSelect = buttonSelect.getScaledCopy(buttonWidth, buttonHeight);
			}

			setMarkerPos(); // get most recent flight positions
			g.setColor(markerColor);

			// {!} constrain positions

			if (flight.isCommandable()) {
				
				// draw altitude slider and labels
				
				drawImage(altitudeBase, new Point2D.Float(altPos.x + sliderWidth, altPos.y));
				
				// account for image mispositioning after rotation
				if (altMarkerPos.y == constrain(altMarkerPos.y, altPos.y, altPos.y + altitudeSize)) {
					drawLine(g, altMarkerPos.x, altMarkerPos.y, altMarkerPos.x + sliderWidth, altMarkerPos.y);
				}
				
				// centred on bottom left edge of slider
				drawString(String.valueOf(flight.getMinAltitude()) + " ft", labelFont, labelColor, altPos.x, altPos.y + altitudeSize);
				// centred on top left edge of slider
				drawString(String.valueOf(flight.getMaxAltitude()) + " ft", labelFont, labelColor, altPos.x, altPos.y);
				
				if (ALTITUDE == mode) { // altitude knob is mid-drag
					drawImage(aIndicatorSelect, altIndicatorPos);
				}
				else {
					drawImage(aIndicator, altIndicatorPos);
				}

				
				// draw speed slider and labels
				
				drawImage(speedBase, speedPos);
				if (speedMarkerPos.x == constrain(speedMarkerPos.x, speedPos.x, speedPos.x + speedSize)) {
					drawLine(g, speedMarkerPos.x, speedMarkerPos.y, speedMarkerPos.x, speedMarkerPos.y + sliderWidth);
				}
				
				// centred on bottom left edge of slider
				drawString(String.valueOf(flight.getMinVelocity()) + " mph", labelFont, labelColor, speedPos.x, speedPos.y + sliderWidth); 
				// centred on top left edge of slider
				drawString(String.valueOf(flight.getMaxVelocity()) + " mph", labelFont, labelColor, speedPos.x + speedSize, speedPos.y + sliderWidth);
				
				if (SPEED == mode) {
					drawImage(aIndicatorSelect, speedIndicatorPos);
				}
				else {
					drawImage(aIndicator, speedIndicatorPos);
				}

	
				// draw heading slider and labels
				
				drawImage(headingBase, headingPos);
				if (HEADING == mode) {
					drawImage(aIndicatorSelect, headingIndicatorPos);
				}
				else {
					drawImage(aIndicator, headingIndicatorPos);
				}
			}

			// draw command button and label
			if (CMD == mode) {
				drawImage(aButtonSelect, cmdPos);
			}
			else /*if (!flight.isTakingOff() && flight.isCommandable())*/ {
				drawImage(aButton, cmdPos);
				
				String cmdString;
				
				if (flight.isGrounded()) {
					cmdString = "Take Off";
				}
				else {
					cmdString = "Land";
				}

				drawString(cmdString, buttonFont, buttonColor,
						cmdPos.x + (buttonWidth / 2.0f),
						cmdPos.y + (buttonHeight / 2.0f));
			}

			if (flight.isCommandable()) {
				// draw abort button and label
				if (ABORT == mode) {
					drawImage(aButtonSelect, abortPos);
				}
				else {
					drawImage(aButton, abortPos);
				}
				drawString("Abort", buttonFont, buttonColor,
						abortPos.x + (buttonWidth / 2.0f),
						abortPos.y + (buttonHeight / 2.0f));
			}

		}
	}
	
	// utility functions

	private void drawImage(Image image, Point2D pos) {
		image.draw((float) (pos.getX() + flight.getX()),
				(float) (pos.getY() + flight.getY()));
	}

	private void drawString(String str, TrueTypeFont font, Color color, float x, float y) {
		font.drawString(
				(float) (x - (font.getWidth(str) / 2.0) + flight.getX()),
				(float) (y - (font.getHeight() / 2.0) + flight.getY()),
				str,
				color
				);
	}

	private void drawLine(Graphics g, float x1, float y1, float x2, float y2) {
		float ox = (float) flight.getX(), oy = (float) flight.getY();
		g.drawLine(x1 + ox, y1 + oy, x2 + ox, y2 + oy);
	}

	private void position() {
		mode = NONE; // invalidate any current mouse movements

		double r = headingSize / 2.0;

		// base position at centre of heading slider
		headingPos.setLocation(-r, -r);

		// position altitude slider to left of heading slider, centred
		altPos.setLocation(-r - spacingSize - sliderWidth, -altitudeSize / 2.0);

		// position speed slider to below heading slider, centred
		speedPos.setLocation(-speedSize / 2.0, r + spacingSize);

		// position buttons to left of altitude slider, centred
		cmdPos.setLocation(
				altPos.x - spacingSize - buttonWidth,
				-spacingSize / 2.0 - buttonHeight
				);
		abortPos.setLocation(
				altPos.x - spacingSize - buttonWidth,
				spacingSize / 2.0
				);

		setIndicatorPos();
	}

	/*
	 * Set the positions of of all of the markers (altitude, and speed) for this FlightMenu.
	 */
	private void setMarkerPos() {
		if (flight != null) {
			altMarkerPos.setLocation(
					// rescale from altitude to pixels
					altPos.x,
					altPos.y + multScale(
							normalScale(
									flight.getAltitude(),
									flight.getMinAltitude(),
									flight.getMaxAltitude()
									),
							altitudeSize,
							0
							)
					);
			speedMarkerPos.setLocation(
					// rescale from velocity to pixels
					speedPos.x + multScale(
							normalScale(
									flight.getVelocity(),
									flight.getMinVelocity(),
									flight.getMaxVelocity()
									),
							0,
							speedSize
							),
					speedPos.y
					);
		}
	}

	/*
	 * Set the positions of of all of the indicators (altitude, speed and heading)
	 * for this FlightMenu.
	 */
	private void setIndicatorPos() {
		if (flight != null) {
			// slider half-widths ("radii")
			double hr = headingSize / 2.0,
					sr = sliderWidth / 2.0,
					ir = indicatorSize / 2.0;

			altIndicatorPos.setLocation(
					altPos.x + sr - ir,
					altPos.y+ multScale(altIndicator, altitudeSize, 0) - ir
					);
			speedIndicatorPos.setLocation(
					speedPos.x + multScale(speedIndicator, 0, speedSize) - ir,
					speedPos.y + sr - ir
					);
			headingIndicatorPos.setLocation(
					(hr - sr) * Math.sin(headingIndicator) - ir,
					(hr - sr) * -Math.cos(headingIndicator) - ir
					);
		}
	}

	/*
	 * Converts a value  on the scale min-max, normalised to 0-1
	 * 
	 * @param pos value to return the position of
	 * @param min lower bound of the input scale
	 * @param max upper bound of the input scale
	 * @return    double precision float, between 0 and 1 if the pos is within the bounds of min and max
	 */
	private double normalScale(double pos, double min, double max) {
		return (pos - min) / (max - min);
	}

	/*
	 * Returns the actual position, on the scale min-max, of a normalised position.
	 */
	private double multScale(double normPos, double min, double max) {
		return min + (normPos * (max - min));
	}

	/*
	 * "Constrains" an integer to no less than min and no more than max.
	 * 
	 * @return value no more than min, no more than max, otherwise val
	 */
	private int constrain(int val, int min, int max) {
		return (val <= min) ? min : (val >= max) ? max : val;
	}

	/*
	 * "Constrains" a double to no less than min and no more than max.
	 * 
	 * @return value no more than min, no more than max, otherwise val
	 */
	private double constrain(double val, double min, double max) {
		return (val <= min) ? min : (val >= max) ? max : val;
	}

	private Boolean inIndicator(Point2D pos, int mouseX, int mouseY) {
		int x = (int) Math.round(pos.getX()), y = (int) Math.round(pos.getY());
		// normalise to internal coordinates
		mouseX -= flight.getX();
		mouseY -= flight.getY();

		return (mouseX > x && mouseX < (x + indicatorSize) && mouseY > y && mouseY < (y + indicatorSize));
	}

	/*
	 * Is one point (mouseX and mouseY) within buttonWidth and buttonHeight 
	 * of another point (the known position of a button)?
	 * 
	 * @see {@link #inRectangle(Point2D, int, int)}
	 * 
	 * @param pos    Point2D, the position of the button in question
	 * @param mouseX Horizontal position of a point (where the mouse was clicked)
	 * @param mouseY Vertical position of point (where the mouse was clicked)
	 * @return       true if pos is within the button's bounds, false if it isn't
	 */
	private Boolean inButton(Point2D pos, int mouseX, int mouseY) {		
		return inRectangle(pos, buttonWidth, buttonHeight, mouseX, mouseY);
	}
	
	/*
	 * Is one point (mouseX and mouseY) within buttonWidth and buttonHeight 
	 * of another point (the known position of a button)?
	 * 
	 * @param pos    Point2D, the position of the button in question
	 * @param mouseX Horizontal position of a point (where the mouse was clicked)
	 * @param mouseY Vertical position of point (where the mouse was clicked)
	 * @return       true if pos is within the button's bounds, false if it isn't
	 */
	private boolean inRectangle(Point2D pos, int width, int height, int mouseX, int mouseY) {
		int x = (int) Math.round(pos.getX()), y = (int) Math.round(pos.getY());
		
		// normalise to internal coordinates
		mouseX -= flight.getX();
		mouseY -= flight.getY();

		return (mouseX > x && mouseX < (x + width) && mouseY > y && mouseY < (y + height));
	}
	
	private void eventTargetAltitude(double altitude) {
		int targetAltitude = (int) Math.round(multScale(altitude,
				flight.getMinAltitude(), flight.getMaxAltitude()));
		flight.setTargetAltitude((int) Math.round(targetAltitude));
	}

	private void eventTargetSpeed(double speed) {
		double targetSpeed = multScale(speed, flight.getMinVelocity(),
				flight.getMaxVelocity());
		flight.setTargetVelocity(targetSpeed);
		// {!} nothing available to change at this time
	}

	private void eventTargetHeading(double heading) {
		int targetHeading = (int) (Math.round(Math.toDegrees(heading)));
		flight.giveHeading(targetHeading); // NOT setTargetHeading
	}

	private void eventAbort() {
		//TODO decide what do do here
	}

	private void eventLand() {
		System.out.println("land");
		// {!} set flight parameters
		flight.land();
	}

	private void eventTakeoff() {
		System.out.println("takeoff");
		flight.takeOff();
		// {!} refresh flight parameters
		flight.getAirspace().getControls().setSelectedFlight(flight);
	}
	
	/*
	 * Moves the active slider's indicator, and changes the flight's altitude, speed or heading accordingly.
	 * 
	 * This should be called when the user drags or clicks to move a slider's indicator.
	 * 
	 * @param indicatorX Horizontal position the slider's indicator was moved to
	 * @param indicatorY Vertical position the slider's indicator was moved to
	 */
	private void moveSlider(int indicatorX, int indicatorY) {
		switch (mode) {
			case ALTITUDE: {
				// calculate y position relative to top of scale
				int flightY = (int) Math.round(indicatorY - flight.getY() - altPos.y);
				// cap y to within bounds of scale
				flightY = constrain(flightY, 0, altitudeSize);
				// invert scale direction
				altIndicator = normalScale(flightY, altitudeSize, 0);
				setIndicatorPos();
				break;
			}
			case SPEED: {
				// calculate x position relative to left of scale
				int flightX = (int) Math.round(indicatorX - flight.getX() - speedPos.x);
				// cap x to within bounds of scale
				flightX = constrain(flightX, 0, speedSize); 
				// invert scale direction
				speedIndicator = normalScale(flightX, 0, speedSize);
				setIndicatorPos();
				break;
			}
			case HEADING: {
				// positions relative to centre of scale
				double x = indicatorX - flight.getX(),
						y = indicatorY - flight.getY();
				headingIndicator = Math.atan2(y, x) + PI / 2; // correct for polar coordinates
				headingIndicator = (headingIndicator < 0)
						? headingIndicator + (2 * PI) 
						: headingIndicator;
				setIndicatorPos();
				break;
			}
		}
	}


	@Override
	public void inputStarted() {
	};

	@Override
	public void inputEnded() {
	};

	@Override
	public boolean isAcceptingInput() {
		return (flight != null);
	}

	@Override
	public void setInput(Input input) {
		// cleanly transfer to new input
		if (this.input != null)
			this.input.removeMouseListener(this);
		this.input = input;
		if (input != null)
			input.addMouseListener(this);
	}

	@Override
	public void mouseMoved(int oldx, int oldy, int newx, int newy) {
	}

	@Override
	public void mouseClicked(int button, int x, int y, int clickCount) {
		if (inRectangle(altPos, sliderWidth, altitudeSize, x, y)) {
			mode = ALTITUDE;
			moveSlider(x, y);
		} else if (inRectangle(speedPos, speedSize, sliderWidth, x, y)) {
			mode = SPEED;
			moveSlider(x, y);
		}
		else {
			//TODO check if click was in the circular heading slider. Use mathematics.
			//      Someone's clearly worked out how to do it somewhere in this code.
		}
	}
	

	@Override
	public void mousePressed(int button, int x, int y) {
		if (Input.MOUSE_LEFT_BUTTON == button) {
			// check for which component mouse is in (if any)
			if (inIndicator(altIndicatorPos, x, y))
				mode = ALTITUDE;
			else if (inIndicator(speedIndicatorPos, x, y))
				mode = SPEED;
			else if (inIndicator(headingIndicatorPos, x, y))
				mode = HEADING;
			else if (inButton(cmdPos, x, y))
				mode = CMD;
			else if (inButton(abortPos, x, y))
				mode = ABORT;
			else
				mode = NONE;
		}
	}

	@Override
	public void mouseDragged(int oldx, int oldy, int newx, int newy) {
		switch (mode) {
		case NONE: // nothing to check
			break;

		case ALTITUDE:
		case SPEED:
		case HEADING:
			moveSlider(newx, newy);
			break;

		// check if should invalidate button presses
		case CMD:
			if (!inButton(cmdPos, newx, newy))
				mode = NONE;
			break;
		case ABORT:
			if (!inButton(abortPos, newx, newy))
				mode = NONE;
			break;
		}
	}

	@Override
	public void mouseReleased(int button, int x, int y) {
		// disable invalid commands
		if ((mode != CMD) && !flight.isCommandable())
			mode = NONE;

		if (Input.MOUSE_LEFT_BUTTON == button) {
			switch (mode) {
			case NONE: // nothing to check
				break;

			// release sliders
			case ALTITUDE:
				eventTargetAltitude(altIndicator);
				break;
			case SPEED:
				eventTargetSpeed(speedIndicator);
				break;
			case HEADING:
				eventTargetHeading(headingIndicator);
				break;

			// release buttons
			case CMD:
				// interpret context
				if (flight.getAltitude() == 0)
					eventTakeoff();
				else
					eventLand();
				break;
			case ABORT:
				eventAbort();
				break;
			}
			mode = NONE;
			setIndicatorPos();
		}
	}

	@Override
	public void mouseWheelMoved(int change) {
	}

	public int getAltSize() {
		return altitudeSize;
	}

	public void setAltSize(int altSize) {
		this.altitudeSize = altSize;
		altitudeBase = null;
		position();
	}

	public int getSpeedSize() {
		return speedSize;
	}

	public void setSpeedSize(int speedSize) {
		this.speedSize = speedSize;
		speedBase = null;
		position();
	}

	public int getBearingSize() {
		return headingSize;
	}

	public void setHeadingSize(int headingSize) {
		this.headingSize = headingSize;
		headingBase = null;
		position();
	}

	public int getSliderWidth() {
		return sliderWidth;
	}

	public void setSliderWidth(int sliderWidth) {
		this.sliderWidth = sliderWidth;
		altitudeBase = null;
		speedBase = null;
		position();
	}

	public int getIndicatorSize() {
		return indicatorSize;
	}

	public void setIndicatorSize(int indicatorSize) {
		this.indicatorSize = indicatorSize;
		aIndicator = null;
		aIndicatorSelect = null;
		position();
	}

	public int getButtonWidth() {
		return buttonWidth;
	}

	public void setButtonWidth(int buttonWidth) {
		this.buttonWidth = buttonWidth;
		aButton = null;
		aButtonSelect = null;
		position();
	}

	public int getButtonHeight() {
		return buttonHeight;
	}

	public void setButtonHeight(int buttonHeight) {
		this.buttonHeight = buttonHeight;
		aButton = null;
		aButtonSelect = null;
		position();
	}

	public int getSpacingSize() {
		return spacingSize;
	}

	public void setSpacingSize(int spacingSize) {
		this.spacingSize = spacingSize;
		position();
	}

	public TrueTypeFont getLabelFont() {
		return labelFont;
	}

	public void setLabelFont(TrueTypeFont labelFont) {
		this.labelFont = labelFont;
	}

	public TrueTypeFont getButtonFont() {
		return buttonFont;
	}

	public void setButtonFont(TrueTypeFont buttonFont) {
		this.buttonFont = buttonFont;
	}

	public Color getLabelColor() {
		return labelColor;
	}

	public void setLabelColor(Color labelColor) {
		this.labelColor = labelColor;
	}

	public Color getButtonColor() {
		return buttonColor;
	}

	public void setButtonColor(Color buttonColor) {
		this.buttonColor = buttonColor;
	}

	public Color getMarkerColor() {
		return markerColor;
	}

	public void setMarkerColor(Color markerColor) {
		this.markerColor = markerColor;
	}

	public void setFlight(Flight flight) {
		mode = NONE;
		this.flight = flight;
		if (flight != null) {
			altIndicator = normalScale(
					flight.getTargetAltitude(),
					flight.getMinAltitude(),
					flight.getMaxAltitude()
					);
			speedIndicator = normalScale(
					flight.getTargetVelocity(),
					flight.getMinVelocity(),
					flight.getMaxVelocity()
					);
			headingIndicator = Math.toRadians(flight.getTargetHeading());
			setIndicatorPos();
		}
	}

}
