## Getting started guide

First, clone the git repository to somewhere on your computer.

Next, import the project into Eclipse.  Note that the project is contained
within the `Game` subfolder of the `assessment-4` repository.

### Running the project

Eclipse might complain that there are errors in the project, because
the project depends on some dependencies (lwjgl, slick2d, and jorbis)
which need to be added to the build path. You can add things to the
build path by right-clicking on the project in the Package Explorer,
and going to **Properties > Java Build Path**. Make sure all the right
dependencies are listed in the Libraries tab, and ticked in the Order
and Export tab.

If, upon trying to run the project, you are still told that lwjgl is
missing, go to **Run > Run Configurations... > Arguments** and put 
one of these in **VM Arguments** box:

    -Djava.library.path="lib/natives/linux"

or

    -Djava.library.path="lib/natives/macosx"

or

    -Djava.library.path="lib/natives/windows"
